//
//  NodeFollower.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 7/21/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NodeFollower : NSObject

@property CCNode *layerToMove;
@property CCNode *nodeToFollow;

+(void)centerLayer:(CCNode*)layer aroundNode:(CCNode*)node;

+(void)centerWorld:(CCNode*)world onChildNode:(CCNode*)nodeToFollow withSceneLayer:(CCNode*)sceneLayer;

+(void)centerWorld:(CCNode*)world onChildNode:(CCNode*)targetNode;

+(void)centerViewOnLayer:(CCNode*)layer withPointInLayer:(CGPoint)posInLayer;
    
@end
