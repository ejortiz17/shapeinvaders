//
//  TopScreenOffSpawn.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 12/9/15.
//  Copyright © 2015 Apportable. All rights reserved.
//

#import "TopScreenOffSpawn.h"

@implementation TopScreenOffSpawn

-(CGPoint)generateRandSpawnPointInLayer:(CCNode *)layer{
    
    
    // let the y spawn point be higher than the screen
    int y = layer.contentSizeInPoints.height + 30;
    
    // x must be within the width of the box
    int xLowerBound = CGRectGetMinX(layer.boundingBox) + 10;
    int xUpperBound = CGRectGetMaxX(layer.boundingBox) - 10;
    
    // item is within bounds
    int x = xLowerBound + arc4random() % (xUpperBound - xLowerBound);
    
    return ccp(x, y);
    
}

@end
