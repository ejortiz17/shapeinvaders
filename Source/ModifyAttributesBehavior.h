//
//  ModifyAttributesBehavior.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 12/29/15.
//  Copyright © 2015 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GameCharacter.h"

@protocol ModifyAttributesBehavior <NSObject>

@property NSDictionary* modData;

-(void)modifyGameCharacter:(GameCharacter*)gameCharacter;

@end
