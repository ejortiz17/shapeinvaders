//
//  Health1.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 12/29/15.
//  Copyright © 2015 Apportable. All rights reserved.
//

#import "Health1.h"

@implementation Health1

-(void)didLoadFromCCB{
    [super didLoadFromCCB];
    self.gameCharModifier = [[AddOneHealth alloc] init];
}

@end
