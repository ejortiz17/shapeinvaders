//
//  GameOverLayer.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 1/9/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "CCNode.h"
#import "ReplaceScene.h"
#import "StandardSceneReplacement.h"
#import "GameKitHelper.h"
@interface GameOverLayer : CCNode


@property id<ReplaceScene>sceneReplacement;

-(void)playAgain;
-(void)toMainScene;
-(void)showLeaderboard;

@end
