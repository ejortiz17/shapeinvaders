//
//  UniformSetBehavior.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 11/5/15.
//  Copyright © 2015 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ItemOccurrenceSetBehavior.h"

@interface UniformSetBehavior : NSObject <ItemOccurrenceSetBehavior>

@end
