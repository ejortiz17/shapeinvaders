//
//  RandSIItemPicker.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 11/5/15.
//  Copyright © 2015 Apportable. All rights reserved.
//
//  Randomly picks an gameItem from the given gameItem consts

#import <Foundation/Foundation.h>
#import "GameObjectTypes.h"
#import "RandGameObjectPicker.h"
#import "ItemOccurrenceSetBehavior.h"
#import "UniformSetBehavior.h"

#import "AddObjectOccurrenceBehavior.h"
#import "StandardObjectOccurrenceAddition.h"

#import "RandomObjectPickBehavior.h"
#import "StandardRandPickBehavior.h"

@interface RandSIItemPicker : NSObject<RandGameObjectPicker>

@property id<ItemOccurrenceSetBehavior> defaultSetBehavior;
@property id<AddObjectOccurrenceBehavior>defaultObjectOccurrenceAddition;
@property id<RandomObjectPickBehavior> pickBehavior;

// init with only one item of each and one occurrence per item
-(id)initUniform;

-(id)initDefault;

@end
