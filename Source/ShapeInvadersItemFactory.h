//
//  ShapeInvadersItemFactory.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 10/17/15.
//  Copyright © 2015 Apportable. All rights reserved.
//
//  Concrete gameItem factory with GameItemFactory Protocol
//

#import <Foundation/Foundation.h>
#import "GameItemFactory.h"
@interface ShapeInvadersItemFactory : NSObject <GameItemFactory>

@end
