//
//  UpperScreenOffSpawn.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 12/9/15.
//  Copyright © 2015 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SpawnPointGeneratorBehavior.h"


@interface UpperScreenOffSpawn : NSObject <SpawnPointGeneratorBehavior>

@end
