//
//  RandSIEnemyPicker.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 12/5/15.
//  Copyright © 2015 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GameObjectTypes.h"
#import "RandGameObjectPicker.h"
#import "ItemOccurrenceSetBehavior.h"
#import "UniformSetBehavior.h"
#import "EmptySetBehavior.h"

#import "AddObjectOccurrenceBehavior.h"
#import "StandardObjectOccurrenceAddition.h"

#import "RandomObjectPickBehavior.h"
#import "StandardRandPickBehavior.h"


@interface RandSIEnemyPicker : NSObject <RandGameObjectPicker>

@property id<ItemOccurrenceSetBehavior> defaultSetBehavior;
@property id<AddObjectOccurrenceBehavior>defaultObjectOccurrenceAddition;
@property id<RandomObjectPickBehavior> pickBehavior;


// init with a default enemy probabilities: set Hex as only enemy to appear first
-(id)initDefault;

// init with only one item of each and one occurrence per item
-(id)initUniform;


@end
