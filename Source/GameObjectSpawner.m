//
//  GameObjectSpawner.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 8/26/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "GameObjectSpawner.h"
#import "GameProjectile.h"

@implementation GameObjectSpawner

@synthesize forceToApply;
@synthesize isActive;

-(void)spawnFactoryObject:(id<GameObjectFactory>)factory toLayer:(CCNode*)spawnLayer withForce:(CGPoint)force{
    
    GameObject *gameObject = [factory makeGameObject];
    
    //use the GameObjectSpawner's position to place the new gameObject
    gameObject.position = self.position;
    [spawnLayer addChild:gameObject];
    [gameObject.physicsBody applyImpulse:force];
    
}

-(void)spawnFactoryObject:(id<GameObjectFactory>)factory toLayer:(CCNode *)spawnLayer{
    
    GameObject *gameObject = [factory makeGameObject];
    
    //use the GameObjectSpawner's position to place the new gameObject
    gameObject.position = self.position;
    [spawnLayer addChild:gameObject];
    [gameObject.physicsBody applyImpulse:self.forceToApply];

    
}

-(void)spawnGameObjectFromCCB:(NSString*)CCBFile toLayer:(CCNode*)spawnLayer{
    
    GameObject *gameObject = (GameObject*)[CCBReader load:CCBFile];
    CGPoint gameSpawnerPos = [self.parent convertToWorldSpace:self.position];
    gameObject.position = gameSpawnerPos;
    [spawnLayer addChild:gameObject];
    [gameObject.physicsBody applyImpulse:self.forceToApply];
}

-(void)spawnGameObjectFromCCB:(NSString *)CCBFile toLayer:(CCNode *)spawnLayer owner:(GameCharacter *)gameCharacter{
    GameObject *gameObject = (GameObject*)[CCBReader load:CCBFile];
    CGPoint gameSpawnerPos = [self.parent convertToWorldSpace:self.position];
    gameObject.position = gameSpawnerPos;
    [spawnLayer addChild:gameObject];
    [gameObject.physicsBody applyImpulse:self.forceToApply];
}

-(void)spawnProjectileFromCCB:(NSString *)CCBFile toLayer:(CCNode *)spawnLayer shooter:(GameCharacter *)shooter{
    GameProjectile *projectile = (GameProjectile*)[CCBReader load:CCBFile];
    projectile.shooter = shooter;
    CGPoint gameSpawnerPos = [self.parent convertToWorldSpace:self.position];
    projectile.position = gameSpawnerPos;
    [spawnLayer addChild:projectile];
    [projectile.physicsBody applyImpulse:self.forceToApply];
}




@end
