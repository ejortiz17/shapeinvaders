//
//  ScrollingSpaceBG1.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 8/22/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "ScrollingSpaceBG1.h"

@implementation ScrollingSpaceBG1{
    CCNode *_bkg1;
    CCNode *_bkg2;
    CCNode *_bkg3;
    CCNode *_bkg4;
}

-(void)didLoadFromCCB{
    self.scrollBehavior = [[InfiniteImageScroll alloc]init];
    self.backgroundNodes = @[_bkg1,_bkg2,_bkg3,_bkg4];
    [self.scrollBehavior setScrollSpeed:0];
}

-(void)fixedUpdate:(CCTime)delta{
    
    [self.scrollBehavior scrollNodes:self.backgroundNodes withTime:delta];
    
}
@end
