//
//  GameObjectTypes.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 10/17/15.
//  Copyright © 2015 Apportable. All rights reserved.
//s

#ifndef GameObjectTypes_h
#define GameObjectTypes_h

typedef enum{
    kHealth1Item,

    kStarWeaponItem,
    
    kInvincibilityStarItem,
    
    kFireRateIncreaseItem,
    
    // used to get total number of items
    itemTotal
    
}GameItemType;

typedef enum {

    kHexEnemy,
    
    kGruntEnemy,
    
    kKamikazeEnemy,
    
    kAntiHeroEnemy,
    
    // total number of enemies available. Placed last to calc total
    enemyTotal


}GameEnemyType;


#endif /* GameObjectTypes_h */

