//
//  SIGameDataHelper.h
//  Shape Inrush
//
//  Created by Eliud Ortiz on 2/6/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "LocalGameDataHelper.h"
#import <iAd/iAd.h>

@interface SIGameDataHelper : LocalGameDataHelper
+(SIGameDataHelper*)sharedHelper;


-(void)initGameData;


-(BOOL)gameDataSet;
-(void)setAdsRemoved:(BOOL)removeAds;

-(void)setBannerValue:(ADBannerView*)banner;
-(NSValue*)getBannerValue;
@end
