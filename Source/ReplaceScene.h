//
//  ReplaceScene.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 1/9/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ReplaceScene <NSObject>

-(void)replaceScene:(NSString*)sceneName;

@end
