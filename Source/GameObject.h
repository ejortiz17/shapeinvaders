//
//  GameObject.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 7/19/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "CCNode.h"

@interface GameObject : CCNode{
    // initially false, TRUE when it has indeed entered the screen on update
    BOOL _enteredView;
    // property checking if the object is currently in the game view
    BOOL _nowInView;
    
    // Selector BOOLs that turn on and off when they are scheduled
    BOOL _removeSelfScheduled;
    
    

}
// check if object is owned by another
@property (weak) GameObject* owner;

/*Classes that will be used by child classes*/
-(void)didLoadFromCCB;

// viewNode is not the parent node, but the view screen node
@property CCNode* viewNode;

+(GameObject*)newGameObjectFromCCB:(NSString*)CCBFile position:(CGPoint)position velocity:(CGPoint)velocity;

-(BOOL)enteredView;
-(void)setEnteredView:(BOOL)boolVal;

-(BOOL)nowInView;
-(void)setNowInView:(BOOL)boolVal;

// will check the position of the object in order to cue up for removal
-(void)removalCheck;

// add to timer and does the checks to see if object has entered view
-(void)enteredViewCheck:(CCNode*)nodeView;
-(void)nowInViewCheck:(CCNode*)nodeView;

-(void)removeSelf;

-(void)removeSelfAfterDestruction;


// create spawn point for each object
+(CGPoint)generateRandomSpawnPoint;

@end
