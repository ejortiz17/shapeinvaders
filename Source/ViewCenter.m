//
//  ViewCenter.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 7/28/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "ViewCenter.h"
#import "VectorCalculator.h"

@implementation ViewCenter

-(void)centerViewOnPoint:(CGPoint)point inLayer:(CCNode *)layer{
    
    // convert point and layer position to world scene coordinates (in points)
    
    CGPoint pointInScene = [layer.parent convertPositionToPoints:[layer convertToWorldSpace:point]
                                                            type:CCPositionTypePoints ];
    CGPoint layerPosInScene = [layer.parent convertToWorldSpace:layer.positionInPoints];
    
    // get position of view center in points
    CGPoint viewCenter = ccp(CGRectGetMidX([UIScreen mainScreen].bounds), CGRectGetMidY([UIScreen mainScreen].bounds));
    
    
    
}



@end
