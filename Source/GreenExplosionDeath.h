//
//  GreenExplosionDeath.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 1/7/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DeathBehavior.h"

@interface GreenExplosionDeath : NSObject <DeathBehavior>

@end
