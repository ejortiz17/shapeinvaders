//
//  GameObject.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 7/19/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "GameObject.h"

@implementation GameObject{

}



+(GameObject *)newGameObjectFromCCB:(NSString *)CCBFile position:(CGPoint)position velocity:(CGPoint)velocity{
    
    // create gameObject from CCBFile should it exist and initialize position and velocity
    GameObject* gameObject;
    @try {
        gameObject = [CCBReader load:CCBFile];
    }
    @catch (NSException *exception) {
        CCLOG(@"File %@ does not exist.", CCBFile);
    }
    
    gameObject.position = position;
    
    if (gameObject.physicsBody != nil) {
        gameObject.physicsBody.velocity = velocity;
    }
    
    return gameObject;

}




// setters and getters for nowInView and enteredView
-(BOOL)enteredView{
    return _enteredView;
}
-(void)setEnteredView:(BOOL)boolVal{
    _enteredView = boolVal;
}
-(BOOL)nowInView{
    return _nowInView;
}
-(void)setNowInView:(BOOL)boolVal{
    _nowInView = boolVal;
}


//
-(void)removalCheck{
    
    // set up for removal of the object in a few seconds if has already entered view and is offscreen
    if (_enteredView && !_nowInView && !_removeSelfScheduled) {
        [self scheduleOnce:@selector(removeSelf) delay:3];
        _removeSelfScheduled = YES;
    }
    // if currently in view, remove the timer for removal in case the object has bounced back in screen
    else if (_enteredView && _nowInView && _removeSelfScheduled){
        
        // don't remove self
        [self unschedule:@selector(removeSelf)];
        _removeSelfScheduled = NO;
        
    }
}




// entered and nowInView Checks
-(void)enteredViewCheck:(CCNode *)nodeView{
    
    CGPoint worldPos = [self.parent convertToWorldSpace:self.position];
    if (CGRectContainsPoint(nodeView.boundingBox, worldPos)) {
        [self setEnteredView:YES];
    }
}
-(void)nowInViewCheck:(CCNode *)nodeView{
    
    CGPoint worldPos = [self.parent convertToWorldSpace:self.position];
    if (CGRectContainsPoint(nodeView.boundingBox, worldPos)) {
        [self setNowInView:YES];
    }
    else{
        [self setNowInView:NO];
    }
}


// override for subclasses 
-(void)removeSelf{
    self.physicsBody = nil;
    [self removeFromParentAndCleanup:YES];
    CCLOG(@"Object Removed");

}

-(void)fixedUpdate:(CCTime)delta{
    // check for object removal
    [self enteredViewCheck:self.viewNode];
    [self nowInViewCheck:self.viewNode];
    
    [self removalCheck];
    
}

@end
