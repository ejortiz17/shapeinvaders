//
//  GameProjectileFactory.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 9/19/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GameObjectFactory.h"

@interface GameProjectileFactory : NSObject <GameObjectFactory>


@end
