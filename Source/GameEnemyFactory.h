//
//  GameEnemyFactory.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 12/5/15.
//  Copyright © 2015 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Enemy.h"
#import "GameObjectTypes.h"


@protocol GameEnemyFactory <NSObject>

-(Enemy*)makeGameItemFromType:(GameEnemyType)gameEnemyType;


@end
