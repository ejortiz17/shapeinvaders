//
//  StandardObjectOccurrenceAddition.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 11/23/15.
//  Copyright © 2015 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AddObjectOccurrenceBehavior.h"
@interface StandardObjectOccurrenceAddition : NSObject <AddObjectOccurrenceBehavior>

@end
