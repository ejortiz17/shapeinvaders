//
//  StarProjectileItem.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 1/18/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "StarProjectileItem.h"
#import "ChangeWeapon.h"
#import "StringConstants.h"

@implementation StarProjectileItem

-(void)didLoadFromCCB{
    [super didLoadFromCCB];
    self.gameCharModifier = [[ChangeWeapon alloc] init];
    self.gameCharModifier.modData = [NSDictionary dictionaryWithObjectsAndKeys:STAR_PROJECTILE, @"weaponFile",
                                     [NSNumber numberWithInt:50], @"ammoNum",
                                     nil];
}

@end
