//
//  GameKitHelper.m
//  Shape Inrush
//
//  Created by Eliud Ortiz on 2/1/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "GameKitHelper.h"
#import "StringConstants.h"


@interface GameKitHelper ()
<GKGameCenterControllerDelegate> {
    BOOL _gameCenterFeaturesEnabled;
}
@end

@implementation GameKitHelper

#pragma mark Singleton stuff

+(id) sharedGameKitHelper {
    static GameKitHelper *sharedGameKitHelper;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedGameKitHelper =
        [[GameKitHelper alloc] init];
    });
    return sharedGameKitHelper;
}

#pragma mark Player Authentication

-(void) authenticateLocalPlayer {
    
    GKLocalPlayer* localPlayer =
    [GKLocalPlayer localPlayer];
    
    localPlayer.authenticateHandler =
    ^(UIViewController *viewController,
      NSError *error) {
        
        [self setLastError:error];
        
        if ([CCDirector sharedDirector].isPaused){
             [[CCDirector sharedDirector] resume];
        }
        
        
        if (localPlayer.isAuthenticated) {
            _gameCenterFeaturesEnabled = YES;
        } else if(viewController != nil) {
            [[CCDirector sharedDirector] pause];
            [self presentViewController:viewController];
        } else {
            _gameCenterFeaturesEnabled = NO;
        }
    };
}
#pragma mark Property setters

-(void) setLastError:(NSError*)error {
    _lastError = [error copy];
    if (_lastError) {
        NSLog(@"GameKitHelper ERROR: %@", [[_lastError userInfo]
                                           description]);
    }
}

#pragma mark UIViewController stuff

-(UIViewController*) getRootViewController {
    return [UIApplication
            sharedApplication].keyWindow.rootViewController;
}

-(void)presentViewController:(UIViewController*)vc {
    UIViewController* rootVC = [self getRootViewController];
    [rootVC presentViewController:vc animated:YES
                       completion:nil];
}
-(void) submitScore:(int64_t)score
           category:(NSString*)category {
    //1: Check if Game Center
    //   features are enabled
    if (!_gameCenterFeaturesEnabled) {
        CCLOG(@"Player not authenticated");
        return;
    }
    
    //2: Create a GKScore object
    GKScore* gkScore =
    [[GKScore alloc]
     initWithLeaderboardIdentifier:category];
    
    //3: Set the score value
    gkScore.value = score;
    
    //4: Send the score to Game Center
    [GKScore reportScores:@[gkScore] withCompletionHandler:
     ^(NSError* error) {
         
         [self setLastError:error];
         
         BOOL success = (error == nil);
         
         if ([_delegate
              respondsToSelector:
              @selector(onScoresSubmitted:)]) {
             
             [_delegate onScoresSubmitted:success];
         }
     }];
}


-(void)showLeaderboard{
    if (_gameCenterFeaturesEnabled == NO)
        return;
    
    GKGameCenterViewController* leaderboardVC = [[GKGameCenterViewController alloc] init];

    
    if (leaderboardVC != nil){
        
        leaderboardVC.gameCenterDelegate = self;
        leaderboardVC.viewState = GKGameCenterViewControllerStateLeaderboards;
        leaderboardVC.leaderboardIdentifier = HIGH_SCORE_LEADERBOARD_CATEGORY;
        [self presentViewController:leaderboardVC];
    }
}

-(void)gameCenterViewControllerDidFinish:(GKGameCenterViewController *)gameCenterViewController{
    [gameCenterViewController dismissViewControllerAnimated:YES completion:nil];
}

-(void)leaderboardViewControllerDidFinish:(GKGameCenterViewController*)viewController
{

    //UIViewController *vc = viewController.view.window.rootViewController;
    //[vc dismissViewControllerAnimated:YES completion:nil];
    //[viewController dismissViewControllerAnimated:NO completion:nil];
    
    //[viewController release];
    
    NSLog( @"Leaderboard closed." );
    
    //[self dismissModalViewController];
    //[_delegate onLeaderboardViewDismissed];
}


@end