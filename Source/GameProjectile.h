//
//  GameProjectile.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 8/31/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "GameObject.h"
#import "GameCharacter.h"

@interface GameProjectile : GameObject

// where the projectile will spawn

// indicates how much damage a projectile can do
@property int damageStrength;

// the shooter of the projectile
@property (weak) CCNode* shooter;


@end
