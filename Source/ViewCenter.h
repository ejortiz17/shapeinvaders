//
//  ViewCenter.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 7/28/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//
// Class used to center a view on a CCNode


#import <Foundation/Foundation.h>

@interface ViewCenter : NSObject

-(void)centerViewOnPoint:(CGPoint)point inLayer:(CCNode*)layer;

@end
