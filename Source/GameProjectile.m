//
//  GameProjectile.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 8/31/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "GameProjectile.h"

@implementation GameProjectile

@synthesize damageStrength = _damageStrength;

-(void)didLoadFromCCB{
    // set a timer to remove bullet after a few seconds
    [self schedule:@selector(removeSelf) interval:3];
    
    self.damageStrength = 1;
    
    self.physicsBody.collisionType = @"projectile";
    self.physicsBody.collisionCategories = @[@"projectiles"];
    self.physicsBody.collisionMask = @[@"enemies", @"projectiles", @"heroes", @"enemies_2"];

    
}

-(void)removeSelf{
    [self removeFromParentAndCleanup:YES];
    //CCLOG(@"Removed Projectile");
}


@end
