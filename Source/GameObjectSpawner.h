//
//  GameObjectSpawner.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 8/26/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//
//  Desc: Spawns gameobjects and eventually shoots them out
//
#import <Foundation/Foundation.h>
#import "GameObject.h"
#import "GameCharacter.h"
#import "GameObjectFactory.h"

@interface GameObjectSpawner : CCNode

@property CGPoint forceToApply;
@property BOOL isActive;

-(void)spawnFactoryObject:(id<GameObjectFactory>)factory toLayer:(CCNode*)spawnLayer withForce:(CGPoint)force;

-(void)spawnFactoryObject:(id<GameObjectFactory>)factory toLayer:(CCNode *)spawnLayer;

-(void)spawnGameObjectFromCCB:(NSString*)CCBFile toLayer:(CCNode*)spawnLayer;

-(void)spawnGameObjectFromCCB:(NSString *)CCBFile toLayer:(CCNode *)spawnLayer owner:(GameCharacter*)gameCharacter;



// shoot game projectiles and assign a shooter to the projectile
-(void)spawnProjectileFromCCB:(NSString*)CCBFile toLayer:(CCNode*)spawnLayer shooter:(GameCharacter*)shooter;

@end
