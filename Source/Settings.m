//
//  Settings.m
//  Shape Inrush
//
//  Created by Eliud Ortiz on 2/14/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "Settings.h"
#import "SIAudioManager.h"

@implementation Settings{

    CCSlider* _sfxVolumeSlider;
    CCSlider* _bgmVolumeSlider;
    CCButton* _btnMainScene;
    
}


-(void)didLoadFromCCB{
    
    if ([SIAudioManager sharedAudioManager] == nil) {
        _sfxVolumeSlider.sliderValue = 0.0;
        _bgmVolumeSlider.sliderValue = 0.0;
    } else{
        
        _sfxVolumeSlider.sliderValue = [SIAudioManager sharedAudioManager].sfxVolume;
        _bgmVolumeSlider.sliderValue = [SIAudioManager sharedAudioManager].bgmVolume;
    }
}

-(void)sfxVolumeChange{
    if ([SIAudioManager sharedAudioManager]!=nil) {
        [SIAudioManager sharedAudioManager].sfxVolume = _sfxVolumeSlider.sliderValue;
    }
}

-(void)bgmVolumeChange{
    if ([SIAudioManager sharedAudioManager]!= nil) {
        [SIAudioManager sharedAudioManager].bgmVolume = _bgmVolumeSlider.sliderValue;
    }
}

-(void)toMainScene{
    CCScene* mainScene = [CCBReader loadAsScene:@"MainScene"];
    [[CCDirector sharedDirector] replaceScene:mainScene];
}


@end
