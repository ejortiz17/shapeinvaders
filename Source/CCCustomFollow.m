//
//  CCCustomFollow.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 8/19/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "CCCustomFollow.h"

@implementation CCCustomFollow

-(void) step:(CCTime) dt
{
    if(_boundarySet)
    {
        // whole map fits inside a single screen, no need to modify the position - unless map boundaries are increased
        if(_boundaryFullyCovered)
            return;
        
        CGPoint tempPos = ccpSub( _halfScreenSize, _followedNode.positionInPoints);
        [(CCNode *)_target setPosition:ccp(clampf(tempPos.x, _leftBoundary, _rightBoundary), clampf(tempPos.y, _bottomBoundary, _topBoundary))];
    }
    else
        [(CCNode *)_target setPosition:ccpSub( _halfScreenSize, _followedNode.positionInPoints )];
}

@end
