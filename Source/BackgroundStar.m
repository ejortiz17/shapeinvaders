 //
//  BackgroundStar.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 1/25/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "BackgroundStar.h"

@implementation BackgroundStar

- (void)didLoadFromCCB{
    // generate a random number between 0.0 and 2.0
    float delay = (arc4random() % 2000) / 1000.f;
    // call method to start animation after random delay
    [self performSelector:@selector(shineAnim) withObject:nil afterDelay:delay];
}

-(void)shineAnim{
    [self.animationManager runAnimationsForSequenceNamed:@"ShineAnim"];
}

@end
