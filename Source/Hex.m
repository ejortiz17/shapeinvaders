//
//  Hex.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 12/9/15.
//  Copyright © 2015 Apportable. All rights reserved.
//

#import "Hex.h"
#import "UpperScreenOffSpawn.h"

@implementation Hex

@synthesize speedFactor = _speedFactor;

-(void)didLoadFromCCB{

    [super didLoadFromCCB];
    // change default spawn generator to UpperScreenHalf Spawn
    self.spawnPointGenerator = [[UpperScreenOffSpawn alloc]init];
    self.movementMultiplier = 1;
    
    self.hp = 1;
    self.invincible = false;
    self.isAttacking = false;
    self.weaponBehavior = NULL;
    
    self.maxSpeedFactor = 200;
    
}

-(void)removeSelf{
    self.physicsBody = nil;
    [self removeFromParentAndCleanup:YES];
    CCLOG(@"Hex Removed");
}

-(void)fixedUpdate:(CCTime)delta{
    [super fixedUpdate:delta];
}

// cap speed factor at 400
-(void)setSpeedFactor:(CGFloat)speedFactor{
    if (speedFactor > self.maxSpeedFactor) {
        _speedFactor = self.maxSpeedFactor;
    }else{
        _speedFactor = speedFactor;
    }
}

-(void)move{

    //update the target position to the hero's position
    [self.moveBehavior setStartingPosition:self.position];
    
    if (self.hero != NULL) {
        [self.moveBehavior setTargetPosition:self.hero.position];
    }
    
    [self.moveBehavior moveWithFactor:self.speedFactor];
}

@end
