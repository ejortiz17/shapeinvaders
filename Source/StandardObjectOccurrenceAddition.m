//
//  StandardObjectOccurrenceAddition.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 11/23/15.
//  Copyright © 2015 Apportable. All rights reserved.
//

#import "StandardObjectOccurrenceAddition.h"

@implementation StandardObjectOccurrenceAddition

-(void)addOccurrenceValue:(int)numToAdd toObjectType:(int)objectType inCollection:(NSMutableArray *)collection{
    
    int currentOccurrence = 0;
    // get occurrence value at index...if exists
    
    if ([collection count]>objectType) {
        currentOccurrence = [[collection objectAtIndex:objectType]intValue];
        // add to current occurrence
        currentOccurrence += numToAdd;
        [collection replaceObjectAtIndex:objectType withObject:[NSNumber numberWithInt:currentOccurrence]];
        
    }
    else{
        [collection setObject:[NSNumber numberWithInt:currentOccurrence] atIndexedSubscript:objectType];
    }
    
    @try {
        currentOccurrence = [[collection objectAtIndex:objectType]intValue];
    }
    @catch (NSException *exception) {
        CCLOG(@"Standard Object Occurrence couldn't be found at index (ObjectType)");
    }
    
    
}


@end
