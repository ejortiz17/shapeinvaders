//
//  VectorCalculator.h
//  ShapeInvaders
//
//
//
//  Created by Eliud Ortiz on 7/19/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//
//  Helps perform repetitive vector calulations 
//

#import <Foundation/Foundation.h>

@interface VectorCalculator : NSObject

// calculate vector starting from the start point and ending at the end point
+(CGPoint)vectorFromStartPoint:(CGPoint)startPoint toEndPoint:(CGPoint)endPoint;

@end
