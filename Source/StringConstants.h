//
//  StringConstants.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 1/4/16.
//  Copyright © 2016 Apportable. All rights reserved.
//
#import <Foundation/Foundation.h>

#ifndef StringConstants_h
#define StringConstants_h

extern NSString *const HERO_DEATH_NOTIFICATION;
extern NSString *const ENEMY_DEATH_NOTIFICATION;
extern NSString *const WEAPON_FIRE_NOTIFICATION;
extern NSString *const ITEM_PICKUP_NOTIFICATION;




extern NSString *const PRESENT_AUTHENTICATION_VIEW_CONTROLLER;
extern NSString *const AUTHENTICATED_NOTIFICATION;
extern NSString *const UNAUTHENTICATED_NOTIFICATION;

extern NSString *const HIGH_SCORE_LEADERBOARD_CATEGORY;


extern NSString *const POINT_BONUS_KEY;
extern NSString *const SOUND_EFFECTS_KEY;


extern NSString *const ADS_REMOVED;
extern NSString *const LOCAL_GAMEDATA_SET;
extern NSString *const BANNER_VALUE;



/****     WEAPONS   ****/
extern NSString *const DEFAULT_PROJECTILE;
extern NSString *const STAR_PROJECTILE;



/****SOUND FILES**/

extern NSString *const BG_MUSIC;
extern NSString *const WEAPON_EFFECT_1;
extern NSString *const WEAPON_EFFECT_2;
extern NSString *const EXPLOSION;
extern NSString *const COLLISION;
extern NSString *const ITEM_PICK_UP;




#endif /* StringConstants_h */
