//
//  GameItem.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 10/17/15.
//  Copyright © 2015 Apportable. All rights reserved.
//
// parent class for all items such as powerups and health

#import "GameItem.h"
#import "SingleImpulseMovement.h"

@implementation GameItem{
    
    
}

@synthesize spawnPointGenerator = _spawnPointGenerator;

-(void)didLoadFromCCB{
    // set physics collision properties
    self.physicsBody.collisionType = @"item";
    self.physicsBody.collisionCategories = @[@"items"];
    self.physicsBody.collisionMask = @[@"items",@"heroes"];
    
    
    [self setEnteredView:NO];
    [self setNowInView:NO];
    
    // by default, all enemies spawn off top of screen
    self.spawnPointGenerator = [[TopScreenOffSpawn alloc]init];
    
    self.moveBehavior = [[SingleImpulseMovement alloc]init];
    [self.moveBehavior setBody:self.physicsBody];
    
    self.speedFactor = 50.0f;

}
// override for subclasses
-(void)removeSelf{
    [self removeFromParentAndCleanup:YES];
    CCLOG(@"Item Removed");
    
}

-(CGPoint)generateRandSpawnPointInLayer:(CCNode *)layer{
    
    return [self.spawnPointGenerator generateRandSpawnPointInLayer:layer];
}

-(void)move{
    
    [self.moveBehavior moveWithFactor:self.speedFactor];
    
}

-(void)modifyGameCharacter:(GameCharacter *)gameCharacter{
    [self.gameCharModifier modifyGameCharacter:gameCharacter];
}

@end
