//
//  Grunt.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 1/20/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "Grunt.h"
#import "TopScreenOffSpawn.h"

@implementation Grunt

@synthesize speedFactor = _speedFactor;


-(void)didLoadFromCCB{
    
    [super didLoadFromCCB];
    // change default spawn generator to top screen Spawn
    self.spawnPointGenerator = [[TopScreenOffSpawn alloc]init];
    self.movementMultiplier = 1;
    
    self.hp = 1;
    self.invincible = false;
    self.isAttacking = false;
    self.weaponBehavior = nil;
    self.maxSpeedFactor = 200;
    
}

// cap speed factor at 250
-(void)setSpeedFactor:(CGFloat)speedFactor{
    if (speedFactor > self.maxSpeedFactor) {
        _speedFactor = self.maxSpeedFactor;
    }else{
        _speedFactor = speedFactor;
    }
}

-(void)removeSelf{
    self.physicsBody = nil;
    [self removeFromParentAndCleanup:YES];
    CCLOG(@"Grunt Removed");
}

-(void)fixedUpdate:(CCTime)delta{
    [super fixedUpdate:delta];
}

-(void)move{
    
    //update the target position to the hero's position
    [self.moveBehavior setStartingPosition:self.position];
    
    [self.moveBehavior setTargetPosition:ccp(self.position.x, 0)];
    
    [self.moveBehavior moveWithFactor:self.speedFactor];
}
@end
