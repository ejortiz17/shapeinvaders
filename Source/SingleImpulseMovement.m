//
//  SingleImpulseMovement.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 12/13/15.
//  Copyright © 2015 Apportable. All rights reserved.
//

#import "SingleImpulseMovement.h"
#import "VectorCalculator.h"
@implementation SingleImpulseMovement

@synthesize body;
@synthesize startingPosition;
@synthesize targetPosition;

// apply single linear impulse
-(void)moveWithFactor:(CGFloat)speed{
    
    // don't apply impulse if the body is already in motion
    
    if (CGPointEqualToPoint(self.body.velocity, ccp(0, 0)) ){
        // get general direction of the body and normalize
        CGPoint impulseVec = [VectorCalculator vectorFromStartPoint:self.startingPosition toEndPoint:self.targetPosition];
        impulseVec = ccpNormalize(impulseVec);
        
        CGPoint impulseVecMult = ccpMult(impulseVec, speed);
        
        [self.body applyImpulse:impulseVecMult];
    }
}



@end
