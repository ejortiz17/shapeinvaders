//
//  GameOverLayer.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 1/9/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "GameOverLayer.h"

@implementation GameOverLayer

@synthesize sceneReplacement = _sceneReplacement;

-(void)didLoadFromCCB{
    
    self.sceneReplacement = [[StandardSceneReplacement alloc] init];
    
}

-(void)playAgain{
    [self.sceneReplacement replaceScene:@"Gameplay"];
}

-(void)toMainScene{
    [self.sceneReplacement replaceScene:@"MainScene"];
}

-(void)showLeaderboard{
    
    CCLOG(@"Leaderboard button pressed");
    GameKitHelper *gkHelper = [GameKitHelper sharedGameKitHelper];
    [gkHelper showLeaderboard];
    
}

@end
