//
//  StandardExplosionDeath.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 12/26/15.
//  Copyright © 2015 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DeathBehavior.h"


@interface StandardExplosionDeath : NSObject <DeathBehavior>

@end
