//
//  DefaultWeaponBehavior.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 8/25/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WeaponBehavior.h"

// default weapon behavior fires projectiles


@interface DefaultWeaponBehavior : CCNode <WeaponBehavior>
-(id)initWithSpawnPoints:(NSMutableArray*)spawnPoints weaponUser:(CCNode*)weaponUser;
-(id)initDefault:(CCNode *)weaponUser;
@end
