//
//  Hero.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 7/19/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "Hero.h"
#import "ZeroInMovement.h"
#import "DefaultWeaponBehavior.h"
#import "GameObjectSpawner.h"
#import "GameProjectileFactory.h"
#import "StandardExplosionDeath.h"
#import "GreenExplosionDeath.h"
#import "StringConstants.h"

@implementation Hero{
    //GameObjectSpawner* _projSpawn1;
}

@synthesize moveBehavior;

@synthesize hp = _hp;

-(void)didLoadFromCCB{
    
    self.hp = 3;
    self.invincible = NO;
    self.takingDamage = NO;
    self.invincibleTimer = 0;
    self.speedFactor = 10;
    self.numOfAnimationsRunning = 0;
    
    // set physics collision properties
    self.physicsBody.collisionType = @"hero";
    self.physicsBody.collisionCategories = @[@"heroes"];
    self.physicsBody.collisionMask = @[@"items",@"projectiles",@"enemies", @"enemies_2"];
    
    [self setMoveBehavior:[[ZeroInMovement alloc]init] ];
    
    [self.moveBehavior setBody:self.physicsBody];
    [self.moveBehavior setStartingPosition:self.position];
    [self.moveBehavior setTargetPosition:self.position];
    
    self.weaponBehavior = [[DefaultWeaponBehavior alloc]initDefault:self];

    
    // set up death behavior
    self.deathBehavior = [[GreenExplosionDeath alloc]init];
    [self.deathBehavior setDyingNode:self];
    
    
    
    
    self.physicsBody.collisionType = @"hero";
}


-(void)countdownInvincibileTimer{
    if (self.invincibleTimer > 0) {
        self.invincibleTimer -= 1;
    }
}

-(void)resetWeaponOnAmmoDepletion{
    
    // if weapon name is empty or is not default and is out of ammo, replace with default ammo
    if ( (self.weaponBehavior.weaponFile==nil || ![self.weaponBehavior.weaponFile isEqualToString:DEFAULT_PROJECTILE]) && self.weaponBehavior.ammo <= 0) {
        self.weaponBehavior.weaponFile = DEFAULT_PROJECTILE;
    }
}

-(void)setHp:(int)hp{
    // don't allow more than 3 lives for a hero
    if(hp>= 3){
        _hp = 3;
    }
    else if (hp<=0){
        _hp = 0;
    }
    else{
        _hp = hp;
    }
}

-(void)move{
    [self.moveBehavior moveWithFactor:self.speedFactor];
}

-(void)useWeapon{
    [self.weaponBehavior useWeapon];
}

-(void)takeDamage:(int)damage{
    
    if (!self.invincible && damage >=1) {
        self.hp -= damage;
        
        // only run damaged animation if hp isn't depleted
        if (self.hp > 0) {
            self.invincible = YES;
            self.takingDamage = TRUE;
            
            //self.invincibleTimer = 1;
        }

    }
}

-(void)outputString: (NSString*)string{
    
    CCLOG(@"%@", string);
    
}

/********** Animations ****************/

-(void)invincibilityCheck{
    if (self.invincible) {
        
        NSString* name = self.animationManager.runningSequenceName;
        
        // hero becomes invulnerable for a second after taking damage
        if (self.takingDamage && self.animationManager.runningSequenceName == nil) {
            [self.animationManager runAnimationsForSequenceNamed:@"TakingDamage"];
            //self.numOfAnimationsRunning +=1;
            [self.animationManager setCompletedAnimationCallbackBlock:^(id sender){
                if ([self.animationManager.lastCompletedSequenceName isEqualToString:@"TakingDamage"]) {
                    self.takingDamage = NO;
                    self.invincible = NO;
                }
            }];
            
        }
        else if (!self.takingDamage){
            
            if (![self.animationManager.runningSequenceName isEqualToString:@"Invincibility"] || self.invincibleTimer > 0) {
                [self.animationManager jumpToSequenceNamed:@"Invincibility" time:0.0];
                self.invincibleTimer = 0;
            }
            
            //self.numOfAnimationsRunning +=1;
            [self.animationManager setCompletedAnimationCallbackBlock:^(id sender){
                if ([self.animationManager.lastCompletedSequenceName isEqualToString:@"Invincibility"]) {
                    self.invincible = NO;
                    self.takingDamage = NO;
                }
            }];
            
        }
    }

    
}

/********** End Animations ****************/




-(void)removeSelfAfterDestruction{
    [self performDeathBehavior];
    [self removeSelf];
}




// post notification upon death of hero
-(void)postNotificationOnDeath{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:HERO_DEATH_NOTIFICATION object:nil userInfo:@{SOUND_EFFECTS_KEY:EXPLOSION}];
}



-(void)fixedUpdate:(CCTime)delta{
    
    [self invincibilityCheck];
    
    [self resetWeaponOnAmmoDepletion];

    [self.moveBehavior setStartingPosition:self.position];
    [self move];
    
    if (self.hp <= 0) {
        [self postNotificationOnDeath];
        [self removeSelfAfterDestruction];
    }
}
@end
