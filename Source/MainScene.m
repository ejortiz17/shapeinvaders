#import "MainScene.h"
#import <GameKit/GameKit.h>
#import "SIGameDataHelper.h"
#import "GameKitHelper.h"
#import <iAd/iAd.h>
#import "AdBanner.h"
#import "SIAudioManager.h"
@implementation MainScene{
    CCButton *_btnPlay;
    CCButton *_btnStore;
    CCButton *_btnSettings;
    CCButton *_btnLeaderboard;
}

-(void)didLoadFromCCB{
    
    
    [[SIAudioManager sharedAudioManager]stopBg];
    
    [[SIGameDataHelper sharedHelper]initGameData];
    
    if (![[SIGameDataHelper sharedHelper] adsRemoved]) {
        [[AdBanner sharedAdBanner]initBanner];
    }
    // remove ads if ads have been disabled
    if ([[SIGameDataHelper sharedHelper]adsRemoved]) {
        if ([AdBanner sharedAdBanner].bannerView!=nil && [AdBanner sharedAdBanner].bannerView.superview!=nil) {
            [[AdBanner sharedAdBanner].bannerView removeFromSuperview];
        }
    }
}

-(void)play{
    
    CCScene *gameplayScene = [CCBReader loadAsScene:@"Gameplay"];
    [[CCDirector sharedDirector] replaceScene:gameplayScene];

}

-(void)toStore{
    CCScene *gameplayScene = [CCBReader loadAsScene:@"Store"];
    [[CCDirector sharedDirector] replaceScene:gameplayScene];
}

-(void)toSettings{
    CCScene* settingsScene = [CCBReader loadAsScene:@"Settings"];
    [[CCDirector sharedDirector] replaceScene:settingsScene];
}

-(void)showLeaderboard{
    GameKitHelper *gkHelper = [GameKitHelper sharedGameKitHelper];
    [gkHelper showLeaderboard];
}


@end
