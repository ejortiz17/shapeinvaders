//
//  AntiHero.m
//  Shape Inrush
//
//  Created by Eliud Ortiz on 2/15/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "AntiHero.h"
#import "TopScreenOffSpawn.h"
#import "ZeroInMovement.h"
#import "DefaultWeaponBehavior.h"
#import "GameObjectSpawner.h"

@implementation AntiHero

-(void)didLoadFromCCB{
    
    [super didLoadFromCCB];
    // change default spawn generator to top screen Spawn
    self.spawnPointGenerator = [[TopScreenOffSpawn alloc]init];
    self.movementMultiplier = .5;
    
    // set physics collision properties
    self.physicsBody.collisionType = @"enemy";
    self.physicsBody.collisionCategories = @[@"enemies"];
    self.physicsBody.collisionMask = @[@"heroes",@"projectiles", @"enemies", @"enemies_2"];
    
    [self setMoveBehavior:[[ZeroInMovement alloc]init] ];
    [self.moveBehavior setBody:self.physicsBody];
    
    self.movementMultiplier = 2;
    self.hp = 10;
    self.invincible = false;
    self.isAttacking = false;
    self.maxSpeedFactor = 1;
    
    // set up weapon /////////
    GameObjectSpawner* spawner1 = [[GameObjectSpawner alloc]init];
    spawner1.position = ccp(0, -20);
    spawner1.isActive = YES;
    spawner1.forceToApply = ccp(0, -50);
    
    NSMutableArray* spawners = [NSMutableArray arrayWithObject:spawner1];
    
    [self addChild:spawner1];
    
    self.weaponBehavior = [[DefaultWeaponBehavior alloc]initWithSpawnPoints:spawners weaponUser:self];
    
    [self schedule:@selector(useWeapon) interval:1/self.weaponBehavior.fireRate];
    // end weapon setup /////////////
    
    
}
-(void)removeSelf{
    self.physicsBody = nil;
    [self removeFromParentAndCleanup:YES];
    CCLOG(@"AntiHero Removed");
}

-(void)fixedUpdate:(CCTime)delta{
    [super fixedUpdate:delta];
    [self.moveBehavior setStartingPosition:self.position];
    
    if (self.hero != nil) {
        [self.moveBehavior setTargetPosition:ccp(self.hero.position.x, [CCDirector sharedDirector].viewSize.height - 80)];
    } else{
        [self.moveBehavior setTargetPosition:ccp([CCDirector sharedDirector].viewSize.width/2, [CCDirector sharedDirector].viewSize.height - 80)];
    }
    
    
    [self move];
}

-(void)move{
    
    [self.moveBehavior moveWithFactor:self.movementMultiplier];
}
-(void)useWeapon{
    [self.weaponBehavior useWeapon];
}
@end
