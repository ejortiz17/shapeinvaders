//
//  StandardRandPickBehavior.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 11/23/15.
//  Copyright © 2015 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RandomObjectPickBehavior.h"
@interface StandardRandPickBehavior : NSObject <RandomObjectPickBehavior>

@end
