//
//  StandardRandPickBehavior.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 11/23/15.
//  Copyright © 2015 Apportable. All rights reserved.
//

#import "StandardRandPickBehavior.h"

@implementation StandardRandPickBehavior

-(int)pickRandomObjectTypeinCollection:(NSMutableArray *)collection{
    
    // will add an object number to the array depending on the occurrences
    NSMutableArray *objectArray = [NSMutableArray array];
    
    for (int i = 0; i< [collection count]; i++) {
        
        int occurrences = (int)[(NSNumber*)[collection objectAtIndex:i] intValue];
        
        
        // push an object type num the amount of times it occurs
        while (occurrences > 0) {
            [objectArray addObject:[NSNumber numberWithInt:i]];
            occurrences--;
        }
    }
    
    // generate random int between 0 and object array size
    int randIndex = arc4random_uniform([objectArray count]);
    
    // select item at said index and return it
    int selectedObjType = [(NSNumber*)[objectArray objectAtIndex:randIndex] intValue];
    
    return selectedObjType;

    
    
    
}

@end
