//
//  ZeroInMovement.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 7/19/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "ZeroInMovement.h"
#import "VectorCalculator.h"

@implementation ZeroInMovement

@synthesize body;
@synthesize startingPosition;
@synthesize targetPosition;

// zeroing in requires targetPosition and startingPosition to be UPDATED EVERY FRAME
-(void)moveWithFactor:(CGFloat)speed{
    
    CGPoint velocityVec = [VectorCalculator vectorFromStartPoint:self.startingPosition toEndPoint:self.targetPosition];
    CGPoint velocityVecMult = ccpMult(velocityVec, speed);
    
    self.body.velocity = velocityVecMult;
}



@end
