//
//  AddOneHealth.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 12/29/15.
//  Copyright © 2015 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModifyAttributesBehavior.h"


@interface AddOneHealth : NSObject  <ModifyAttributesBehavior>

@end
