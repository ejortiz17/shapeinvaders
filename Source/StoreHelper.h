//
//  StoreHelper.h
//  Shape Inrush
//
//  Created by Eliud Ortiz on 2/9/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "CCNode.h"
#import <StoreKit/StoreKit.h>

@interface StoreHelper : CCNode <SKProductsRequestDelegate, SKPaymentTransactionObserver>{
    
}

- (IBAction)purchase:(SKProduct *)product;
- (IBAction)restore;
- (IBAction)tapsRemoveAds;

@end
