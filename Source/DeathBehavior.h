//
//  DeathBehavior.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 12/26/15.
//  Copyright © 2015 Apportable. All rights reserved.
//
// animation or action to run upon an item's death
#import <Foundation/Foundation.h>

@protocol DeathBehavior <NSObject>

@property CCNode* dyingNode;
-(void)performDeathBehavior;

@end
