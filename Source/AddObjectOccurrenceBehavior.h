//
//  AddObjectOccurrenceBehavior.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 11/23/15.
//  Copyright © 2015 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol AddObjectOccurrenceBehavior <NSObject>

-(void)addOccurrenceValue:(int)numToAdd toObjectType:(int)objectType inCollection:(NSMutableArray*)collection;
@end
