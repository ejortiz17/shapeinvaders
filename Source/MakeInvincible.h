//
//  MakeInvincible.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 1/13/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModifyAttributesBehavior.h"

@interface MakeInvincible : NSObject <ModifyAttributesBehavior>

@end
