//
//  GameProjectileFactory.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 9/19/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "GameProjectileFactory.h"
#import "GameProjectile.h"
@implementation GameProjectileFactory

-(GameObject*)makeGameObject{
    
    GameProjectile* projectile = [[GameProjectile alloc]init];
    return projectile;
}

@end
