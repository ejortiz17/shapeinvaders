//
//  SingleImpulseMovement.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 12/13/15.
//  Copyright © 2015 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PhysicsBodyMoveBehavior.h"
@interface SingleImpulseMovement : NSObject <PhysicsBodyMoveBehavior>

@end
