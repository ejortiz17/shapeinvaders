//
//  StarProjectile.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 1/18/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "StarProjectile.h"

@implementation StarProjectile

-(void)didLoadFromCCB{
    [super didLoadFromCCB];
    self.damageStrength = 3;
}

@end
