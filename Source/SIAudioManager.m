//
//  SIAudioManager.m
//  Shape Inrush
//
//  Created by Eliud Ortiz on 2/13/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "SIAudioManager.h"
#import "StringConstants.h"

@implementation SIAudioManager{
    
}

@synthesize sfxVolume = _sfxVolume;
@synthesize bgmVolume = _bgmVolume;



+(SIAudioManager*)sharedAudioManager{
    
    static SIAudioManager *_sharedAudioManager = nil;
    
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedAudioManager = [[SIAudioManager alloc] init];
    });
    
    return _sharedAudioManager;
}

-(void)setUp{
    
    self.bgMusicOn = YES;
    self.soundFxOn = YES;
    
    self.sfxVolume = 1.0;
    self.bgmVolume = 0.5;
    
    [self preloadBg:BG_MUSIC];
    
    [self preloadEffect:COLLISION];
    [self preloadEffect:EXPLOSION];
    [self preloadEffect:ITEM_PICK_UP];
    [self preloadEffect:WEAPON_EFFECT_1];
    [self preloadEffect:WEAPON_EFFECT_2];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playEffectFromNotification:) name:HERO_DEATH_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playEffectFromNotification:) name:ENEMY_DEATH_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playEffectFromNotification:) name:WEAPON_FIRE_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playEffectFromNotification:) name:ITEM_PICKUP_NOTIFICATION object:nil];
    
    
}

-(void)setSfxVolume:(float)sfxVolume{
    if (sfxVolume >= 0.0 && sfxVolume <= 1.0) {
        _sfxVolume = sfxVolume;
    }
    else{
        //[NSException raise:@"Out of bounds ex" format:@"Invalid value for SFX Volume: not between 0 and 1."];
        _sfxVolume = 0.0;
    }
}

-(float)getSfxVolume{
    return _sfxVolume;
}

-(void)setBgmVolume:(float)bgmVolume{
    if (bgmVolume >= 0.0 && bgmVolume <= 1.0) {
        _bgmVolume = bgmVolume;
    }
    else{
        //[NSException raise:@"Out of bounds ex" format:@"Invalid value for BGM Volume: not between 0 and 1."];
        _bgmVolume = 0.0;
    }
}

-(float)getBgmVolume{
    return _bgmVolume;
}


-(void)preloadEffect:(NSString*)filename{
    [[OALSimpleAudio sharedInstance] preloadEffect:filename];
}


// preload bg music
-(void)preloadBg:(NSString*)bgMusic{
    [[OALSimpleAudio sharedInstance]preloadBg:bgMusic];
}

-(void)playBgWithLoop:(BOOL)loop{
    if (self.bgmVolume != 0.0) {
        [[OALSimpleAudio sharedInstance]playBgWithLoop:loop];
    }
    
}

-(void)playBg:(NSString*)filePath volume:(float)volume pan:(float)pan loop:(bool)loop{
    
    if (self.bgmVolume != 0.0) {
        [[OALSimpleAudio sharedInstance]playBg:filePath volume:volume pan:pan loop:loop];
    }
}


-(void)stopBg{
    [[OALSimpleAudio sharedInstance]stopBg];
}


-(void)pauseAudio{
    [OALSimpleAudio sharedInstance].paused = YES;
}

-(void)resumeAudio{
    [OALSimpleAudio sharedInstance].paused = NO;
}

-(void)playEffect:(NSString*)filePath{
    if (self.sfxVolume != 0.0) {
        [[OALSimpleAudio sharedInstance] playEffect:filePath];
    }
}

-(void)playEffect:(NSString*)filePath volume:(float)volume pitch:(float)pitch pan:(float)pan loop:(bool)loop{
    
    if (self.sfxVolume != 0.0) {
        [[OALSimpleAudio sharedInstance]playEffect:filePath volume:volume pitch:pitch pan:pan loop:loop];
    }
}

-(void)playEffectFromNotification:(NSNotification*)notification{
    NSString* key = SOUND_EFFECTS_KEY;
    NSDictionary *dicionary = [notification userInfo];
    NSString *stringValueToUse = [dicionary valueForKey:key];
    
    if (stringValueToUse != nil) {
        //[self playEffect:stringValueToUse];
        [self playEffect:stringValueToUse volume:self.sfxVolume pitch:1.0 pan:0.0 loop:NO];
        
    }
    
}




@end
