//
//  PauseMenu.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 1/25/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "CCNode.h"
#import "ReplaceScene.h"

@interface PauseMenu : CCNode 

@property id<ReplaceScene>sceneReplacer;

-(void)toMainScene;
-(void)resumePlay;
+(void)pausePlay;
-(void)restartPlay;

@end
