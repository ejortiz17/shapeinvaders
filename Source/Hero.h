//
//  Hero.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 7/19/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "GameCharacter.h"

#import "PhysicsBodyMoveBehavior.h"

@interface Hero : GameCharacter{
    id<PhysicsBodyMoveBehavior> moveBehavior;
}

@property (strong) id<PhysicsBodyMoveBehavior> moveBehavior;


-(void)move;
-(void)outputString: (NSString*)string;

-(void)postNotificationOnDeath;

@end
