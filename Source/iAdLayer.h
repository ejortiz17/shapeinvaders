//
//  iAdLayer.h
//  Shape Inrush
//
//  Created by Eliud Ortiz on 2/8/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "CCNode.h"
#import <iAd/iAd.h>

@interface iAdLayer : CCNode <ADBannerViewDelegate>{
}

@property ADBannerView *bannerView;
@end
