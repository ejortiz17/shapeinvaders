//
//  MakeInvincible.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 1/13/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "MakeInvincible.h"

@implementation MakeInvincible

-(void)modifyGameCharacter:(GameCharacter *)gameCharacter{
    
    if (gameCharacter!=nil) {
        gameCharacter.takingDamage = NO;
        gameCharacter.invincible = YES;
        gameCharacter.invincibleTimer = 1;
    }
}

@end
