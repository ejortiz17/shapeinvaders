//
//  RandSIItemPicker.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 11/5/15.
//  Copyright © 2015 Apportable. All rights reserved.
//

#import "RandSIItemPicker.h"
#import "EmptySetBehavior.h"

@implementation RandSIItemPicker{
    
    NSMutableArray* _objectsAndOccurrences;
}

@synthesize defaultSetBehavior = _defaultSetBehavior;
@synthesize defaultObjectOccurrenceAddition = _defaultObjectOccurrenceAddition;
@synthesize pickBehavior = _pickBehavior;

-(id)initUniform{
    
    self = [super init];
    
    if (self) {
        
        // set default behavior and set items and occurrences uniformly
        self.defaultSetBehavior = [[UniformSetBehavior alloc] init];
        _objectsAndOccurrences = [NSMutableArray array];
        [self.defaultSetBehavior setItemsAndOccurrences:_objectsAndOccurrences numOfItems:itemTotal];
        
        self.pickBehavior = [[StandardRandPickBehavior alloc]init];
        
        self.defaultObjectOccurrenceAddition = [[StandardObjectOccurrenceAddition alloc]init];
        
    }
    
    return self;
    
}

-(id)initDefault{
    self = [super init];
    
    if (self) {
        self.defaultSetBehavior = [[EmptySetBehavior alloc]init];
        _objectsAndOccurrences = [NSMutableArray array];
        
        [self.defaultSetBehavior setItemsAndOccurrences:_objectsAndOccurrences numOfItems:itemTotal];
        
        self.pickBehavior = [[StandardRandPickBehavior alloc]init];
        self.defaultObjectOccurrenceAddition = [[StandardObjectOccurrenceAddition alloc]init];

        // set initial counts of item occurrences
        [self.defaultObjectOccurrenceAddition addOccurrenceValue:4 toObjectType:kStarWeaponItem inCollection:_objectsAndOccurrences];
        [self.defaultObjectOccurrenceAddition addOccurrenceValue:3 toObjectType:kHealth1Item inCollection:_objectsAndOccurrences];
        [self.defaultObjectOccurrenceAddition addOccurrenceValue:2 toObjectType:kFireRateIncreaseItem inCollection:_objectsAndOccurrences];
        [self.defaultObjectOccurrenceAddition addOccurrenceValue:1 toObjectType:kInvincibilityStarItem inCollection:_objectsAndOccurrences];
    }
    
    return self;
}


// return the random gameObject
-(int)pickRandomObjectType{
    
    return [self.pickBehavior pickRandomObjectTypeinCollection:_objectsAndOccurrences];
}


-(void)addOccurrenceValue:(int)numToAdd toObjectType:(int)objectType{
    
    [self.defaultObjectOccurrenceAddition addOccurrenceValue:numToAdd toObjectType:objectType inCollection:_objectsAndOccurrences];
    
}


@end
