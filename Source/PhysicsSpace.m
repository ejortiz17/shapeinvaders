//
//  PhysicsSpace.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 8/25/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "PhysicsSpace.h"
#import "Enemy.h"
#import "GameProjectile.h"
#import "GameItem.h"
#import "StringConstants.h"

@implementation PhysicsSpace

-(void)didLoadFromCCB{
    //set physicsNode handler
    [self setCollisionDelegate:self];
    
}

-(void)ccPhysicsCollisionPostSolve:(CCPhysicsCollisionPair *)pair hero:(CCNode *)nodeA enemy:(CCNode *)nodeB{
    //CCLOG(@"Hero collided with enemy");
    Hero *hero = (Hero*)nodeA;
    Enemy *enemy = (Enemy*)nodeB;
    
    // subtract one hp from collision
    
    [hero takeDamage:1];
    
    [enemy takeDamage:1];
    
    
    
    
}
-(void)ccPhysicsCollisionPostSolve:(CCPhysicsCollisionPair *)pair item:(CCNode *)nodeA item:(CCNode *)nodeB{
    //CCLOG(@"two items collided");
}

-(void)ccPhysicsCollisionPostSolve:(CCPhysicsCollisionPair *)pair hero:(CCNode *)nodeA item:(CCNode *)nodeB{
    
    Hero* hero = (Hero*)nodeA;
    GameItem* gameItem = (GameItem*)nodeB;
    
    [gameItem modifyGameCharacter:hero];
    
    // play pickup sound
    [[NSNotificationCenter defaultCenter] postNotificationName:ITEM_PICKUP_NOTIFICATION object:nil userInfo:@{SOUND_EFFECTS_KEY:ITEM_PICK_UP}];
    
    [gameItem removeSelf];
    
}

-(void)ccPhysicsCollisionPostSolve:(CCPhysicsCollisionPair *)pair projectile:(CCNode *)nodeA hero:(CCNode *)nodeB{
    
    Hero* hero = (Hero*)nodeB;
    GameProjectile *projectile = (GameProjectile*)nodeA;
    
    
    // only take damage if the projectile did not come from the hero
    if (projectile.shooter != hero) {
        [hero takeDamage:projectile.damageStrength];
        
        [(GameProjectile*)nodeA removeSelf];
    }
    

    
}


-(void)ccPhysicsCollisionPostSolve:(CCPhysicsCollisionPair *)pair projectile:(CCNode *)nodeA enemy:(CCNode *)nodeB{
    
    Enemy* enemy = (Enemy*)nodeB;
    GameProjectile *projectile = (GameProjectile*)nodeA;
    
    
    if (projectile.shooter != enemy) {
        enemy.hp -= projectile.damageStrength;
        
        [(GameProjectile*)nodeA removeSelf];
    }
    

}

-(void)ccPhysicsCollisionPostSolve:(CCPhysicsCollisionPair *)pair enemy_2:(CCNode *)nodeA hero:(CCNode *)nodeB{
    
    Hero* hero = (Hero*)nodeB;
    Enemy *enemy = (Enemy*)nodeA;
    
    [hero takeDamage:1];
    
    // enemy destroys itself
    [enemy takeDamage:enemy.hp];
}

-(void)ccPhysicsCollisionPostSolve:(CCPhysicsCollisionPair *)pair enemy_2:(CCNode *)nodeA projectile:(CCNode *)nodeB{
    Enemy* enemy = (Enemy*)nodeA;
    GameProjectile* projectile = (GameProjectile*)nodeB;
    
    [enemy takeDamage:projectile.damageStrength];
    [projectile removeSelf];
}

-(void)ccPhysicsCollisionPostSolve:(CCPhysicsCollisionPair *)pair enemy_2:(CCNode *)nodeA enemy:(CCNode *)nodeB{
    Enemy* enemy_2 = (Enemy*)nodeA;
    Enemy* enemy_1 = (Enemy*)nodeB;
    
    
    [enemy_1 takeDamage:1];
    [enemy_2 takeDamage:1];
}

-(void)ccPhysicsCollisionPostSolve:(CCPhysicsCollisionPair *)pair enemy_2:(CCNode *)nodeA enemy_2:(CCNode *)nodeB{
    Enemy* enemy_2 = (Enemy*)nodeA;
    Enemy* enemy_1 = (Enemy*)nodeB;
    
    
    [enemy_1 takeDamage:1];
    [enemy_2 takeDamage:1];
}


@end
