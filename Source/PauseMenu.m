//
//  PauseMenu.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 1/25/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "PauseMenu.h"
#import "StandardSceneReplacement.h"
#import "SIAudioManager.h"
@implementation PauseMenu

-(void)didLoadFromCCB{
    self.sceneReplacer = [[StandardSceneReplacement alloc] init];
}

-(void)toMainScene{
    
    [self.sceneReplacer replaceScene:@"MainScene"];
    
}

+(void)pausePlay{
    [[CCDirector sharedDirector]pushScene:(CCScene*)[CCBReader loadAsScene:@"PauseMenu"]];
}

-(void)resumePlay{
    [[CCDirector sharedDirector]popScene];
    [[SIAudioManager sharedAudioManager]resumeAudio];

}

-(void)restartPlay{
    [self.sceneReplacer replaceScene:@"Gameplay"];
}
@end
