//
//  StringConstants.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 1/4/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "StringConstants.h"

NSString *const HERO_DEATH_NOTIFICATION = @"heroDeathNotification";
NSString *const ENEMY_DEATH_NOTIFICATION = @"enemyDeathNotification";
NSString *const WEAPON_FIRE_NOTIFICATION = @"weaponFireNotification";
NSString *const ITEM_PICKUP_NOTIFICATION = @"itemPickupNotification";

NSString *const AUTHENTICATED_NOTIFICATION = @"playerAuthenticated";
NSString *const UNAUTHENTICATED_NOTIFICATION = @"playerUnauthenticated";
NSString *const PRESENT_AUTHENTICATION_VIEW_CONTROLLER = @"present_authentication_view_controller";

NSString *const HIGH_SCORE_LEADERBOARD_CATEGORY = @"SI_LEADERBOARD_001";

NSString* const POINT_BONUS_KEY = @"pointBonus";
NSString* const SOUND_EFFECTS_KEY = @"soundEffects";

NSString* const LOCAL_GAMEDATA_SET = @"localGamedataSet";
NSString* const ADS_REMOVED = @"adsRemoved";
NSString* const BANNER_VALUE = @"bannerValue";



/****SOUND FILES**/

NSString *const BG_MUSIC = @"ShapeInrushThemeLoop.mp3";
NSString *const WEAPON_EFFECT_1 = @"weapon_effect_1.mp3";
NSString *const WEAPON_EFFECT_2 = @"weapon_effect_2.mp3";
NSString *const EXPLOSION = @"explosion.mp3";
NSString *const COLLISION = @"collision.mp3";
NSString *const ITEM_PICK_UP = @"itemPickUp.mp3";

/******************
 ***************************************

 Weapon Strings


***************************************
********************/

NSString* const DEFAULT_PROJECTILE = @"DefaultProjectile";
NSString* const STAR_PROJECTILE = @"StarProjectile";
