//
//  AddOneHealth.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 12/29/15.
//  Copyright © 2015 Apportable. All rights reserved.
//

#import "AddOneHealth.h"
@implementation AddOneHealth


@synthesize modData = _modData;

-(void)modifyGameCharacter:(GameCharacter *)gameCharacter{
    
    if (gameCharacter!=nil) {
        gameCharacter.hp += 1;
    }
}

@end
