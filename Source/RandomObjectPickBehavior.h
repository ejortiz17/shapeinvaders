//
//  RandomObjectPickBehavior.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 11/23/15.
//  Copyright © 2015 Apportable. All rights reserved.
//
//  Picks an Item randomly from a given array
#import <Foundation/Foundation.h>

@protocol RandomObjectPickBehavior <NSObject>

-(int)pickRandomObjectTypeinCollection: (NSMutableArray*)collection;

@end
