//
//  InfiniteImageScroll.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 8/20/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "InfiniteImageScroll.h"

@implementation InfiniteImageScroll

@synthesize scrollSpeed = _scrollSpeed;


-(void)scrollNodes:(NSArray *)backgroundNodes withTime:(CCTime)delta{
    
    // only run the scrolling function if there is more than one background image
    if ([backgroundNodes count] > 1) {
        
        for (CCNode* background in backgroundNodes) {
            
            CGPoint newBGPosInPoints;
            
            // get background postion on screen
            CGPoint backgroundPosInNode = [background.parent convertToNodeSpace:background.positionInPoints];
            
            //background.position = ccp(backgroundPosInNode.x, backgroundPosInNode.y - delta*_scrollSpeed);
            
            if(backgroundPosInNode.y <= -1*background.parent.contentSize.height){
                
                //reset image to a certain height percentage of the parent screen
                newBGPosInPoints = ccp(background.position.x,
                                       ([backgroundNodes count]-1)*background.parent.contentSize.height);
            }
            else{
                newBGPosInPoints = ccp(backgroundPosInNode.x, backgroundPosInNode.y - delta*_scrollSpeed);
            }
            CGPoint newPos = [background convertPositionFromPoints:newBGPosInPoints type:background.positionType];
            background.position = newPos;
            
        }
        
    }
    
    
}

@end
