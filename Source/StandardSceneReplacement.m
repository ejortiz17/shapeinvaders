//
//  StandardSceneReplacement.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 1/9/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "StandardSceneReplacement.h"
#import "ReplaceScene.h"

@implementation StandardSceneReplacement

-(void)replaceScene:(NSString *)sceneName{
    
    CCScene *newScene = [CCBReader loadAsScene:sceneName];
    [[CCDirector sharedDirector]replaceScene:newScene];
}

@end
