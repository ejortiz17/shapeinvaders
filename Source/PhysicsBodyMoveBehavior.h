//
//  PhysicsBodyMoveBehavior.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 7/19/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PhysicsBodyMoveBehavior <NSObject>

@property CCPhysicsBody *body;
// physics body motion depends on starting positions and target positions to create
// velocity and force vectors
@property CGPoint startingPosition;
@property CGPoint targetPosition;

// actual movement implementation
-(void)moveWithFactor:(CGFloat)speed;

@end
