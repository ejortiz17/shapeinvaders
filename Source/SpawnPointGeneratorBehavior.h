//
//  SpawnPointGeneratorBehavior.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 12/9/15.
//  Copyright © 2015 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SpawnPointGeneratorBehavior <NSObject>

-(CGPoint)generateRandSpawnPointInLayer:(CCNode*)layer;

@end
