//
//  ZeroInMovement.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 7/19/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PhysicsBodyMoveBehavior.h"

@interface ZeroInMovement : NSObject <PhysicsBodyMoveBehavior>

@end
