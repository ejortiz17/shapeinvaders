//
//  SIGameDataHelper.m
//  Shape Inrush
//
//  Created by Eliud Ortiz on 2/6/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "SIGameDataHelper.h"
#import "StringConstants.h"


@implementation SIGameDataHelper

// singleton
+(SIGameDataHelper*)sharedHelper{
    
    static SIGameDataHelper *_sharedHelper = nil;
    
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedHelper = [[SIGameDataHelper alloc] init];
    });
    
    return _sharedHelper;
}
-(void)initGameData{
    if(![self isKeyValid:LOCAL_GAMEDATA_SET]){
        [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:LOCAL_GAMEDATA_SET];
        [[NSUserDefaults standardUserDefaults] setBool:FALSE forKey:ADS_REMOVED];
    }
}

-(BOOL)adsRemoved{
    if ([self isKeyValid:ADS_REMOVED]) {
        return [[NSUserDefaults standardUserDefaults]boolForKey:ADS_REMOVED];
    }else{
        return NO;
    }
}

-(void)setAdsRemoved:(BOOL)removeAds{
    if ([self gameDataSet]) {
        [[NSUserDefaults standardUserDefaults]setBool:removeAds forKey:ADS_REMOVED];
    }else{
        CCLOGWARN(@"Local GameData is not set. Before removing ads, set all game data.");
    }
}

-(BOOL)gameDataSet{
    if ([self isKeyValid:LOCAL_GAMEDATA_SET]) {
        return [[NSUserDefaults standardUserDefaults] boolForKey:LOCAL_GAMEDATA_SET];
    }else{
        return NO;
    }
}

-(void)setBannerValue:(ADBannerView *)banner{
    
    //ADBannerView *__strong *bannerPointer = &banner;
    //NSValue *bannerValue = [NSValue valueWithPointer:(__bridge const void*)(*bannerPointer)];
    NSValue *bannerValue = [NSValue valueWithPointer:&banner];
    [[NSUserDefaults standardUserDefaults] setValue:bannerValue forKey:BANNER_VALUE];
}

-(NSValue*)getBannerValue{
    NSValue *value = [[NSUserDefaults standardUserDefaults] valueForKey:BANNER_VALUE];
    return value;
}


@end
