//
//  ShapeInvadersEnemyFactory.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 12/5/15.
//  Copyright © 2015 Apportable. All rights reserved.
//

#import "ShapeInvadersEnemyFactory.h"

@implementation ShapeInvadersEnemyFactory{
}


-(GameObject*)makeGameItemFromType:(GameEnemyType)gameEnemyType{
    
    // create blank gameItem
    Enemy *enemy = [[Enemy alloc]init];
    enemy = nil;
    
    // switch will be the GameItem type
    switch (gameEnemyType) {
        case kHexEnemy:
            enemy = (Enemy*)[CCBReader load:@"Hex"];
            break;
            
        case kGruntEnemy:
            enemy = (Enemy*)[CCBReader load:@"Grunt"];
            break;
        
        case kKamikazeEnemy:
            enemy = (Enemy*)[CCBReader load:@"Kamikaze"];
            break;
            
        case kAntiHeroEnemy:
            enemy = (Enemy*)[CCBReader load:@"AntiHero"];
            break;
            
        default:
            break;
    }
    
    return enemy;
}

@end
