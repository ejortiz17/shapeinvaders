//
//  RandGameObjectPicker.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 11/5/15.
//  Copyright © 2015 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RandGameObjectPicker <NSObject>

// the key of the array is the object(typedef enum or int) and maps to the num of occurrences
//@property NSMutableArray* objectsAndOccurrences;

// each index will hold a type of object
//@property NSMutableArray* allObjects;

-(int)pickRandomObjectType;

// add or subtract the number of occurrences of an object type
-(void)addOccurrenceValue:(int)numToAdd toObjectType:(int)objectType;


@end
