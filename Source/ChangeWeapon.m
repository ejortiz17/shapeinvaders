//
//  ChangeWeapon.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 1/18/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "ChangeWeapon.h"
#import "StringConstants.h"

@implementation ChangeWeapon

@synthesize modData = _modData;

-(void)modifyGameCharacter:(GameCharacter *)gameCharacter{
    if (gameCharacter.weaponBehavior != nil) {
        [self modifyWeapon:gameCharacter.weaponBehavior];
    }
}

-(void)modifyWeapon:(id<WeaponBehavior>)weaponBehavior{
    NSString* weaponFile = [self.modData objectForKey:@"weaponFile"];
    NSNumber* ammoNum = [self.modData objectForKey:@"ammoNum"];
    
    weaponBehavior.ammo = [ammoNum intValue];
    weaponBehavior.weaponFile = weaponFile;
}

@end
