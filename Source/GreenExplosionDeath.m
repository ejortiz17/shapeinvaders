//
//  GreenExplosionDeath.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 1/7/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "GreenExplosionDeath.h"

@implementation GreenExplosionDeath

@synthesize dyingNode = _dyingNode;


-(void)performDeathBehavior{
    CCParticleSystem *stdExplosion = (CCParticleSystem*)[CCBReader load:@"GreenExplosion"];
    // make the particle effect clean itself up, once it is completed
    stdExplosion.autoRemoveOnFinish = TRUE;
    // place the particle effect on the seals position
    stdExplosion.position = _dyingNode.position;
    // add the particle effect to the same node the seal is on
    [_dyingNode.parent addChild:stdExplosion];
    
}

@end
