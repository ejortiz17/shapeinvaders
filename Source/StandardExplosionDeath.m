//
//  StandardExplosionDeath.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 12/26/15.
//  Copyright © 2015 Apportable. All rights reserved.
//

#import "StandardExplosionDeath.h"

@implementation StandardExplosionDeath

@synthesize dyingNode = _dyingNode;
-(void)performDeathBehavior{
    CCParticleSystem *stdExplosion = (CCParticleSystem*)[CCBReader load:@"StandardExplosion"];
    // make the particle effect clean itself up, once it is completed
    stdExplosion.autoRemoveOnFinish = TRUE;
    // place the particle effect on the seals position
    stdExplosion.position = _dyingNode.position;
    // add the particle effect to the same node the seal is on
    [_dyingNode.parent addChild:stdExplosion];

}

@end
