//
//  AdBanner.m
//  Shape Inrush
//
//  Created by Eliud Ortiz on 2/9/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "AdBanner.h"
#import "SIAudioManager.h"


#define IPAD     UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad
#define IPHONE   UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone

@implementation AdBanner

@synthesize bannerView = _bannerView;

+(AdBanner*)sharedAdBanner{
    
    static AdBanner *_sharedBanner = nil;
    
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedBanner = [[AdBanner alloc] init];
    });
    
    return _sharedBanner;
}

-(void)initBanner{
    
    int adFrameHeight;
    if (IPAD) {
        adFrameHeight = 66;
    }else{
        adFrameHeight = 50;
    }
    // On iOS 6 ADBannerView introduces a new initializer, use it when available.
    if ([ADBannerView instancesRespondToSelector:@selector(initWithAdType:)]){
        
        _bannerView = [[ADBannerView alloc] initWithFrame:CGRectMake(0, [CCDirector sharedDirector].view.bounds.size.height - adFrameHeight, 0, 0)];
        // _bannerView = [[ADBannerView alloc] initWithAdType:ADAdTypeBanner];
        
    } else {
        _bannerView = [[ADBannerView alloc] init];
    }
    _bannerView.requiredContentSizeIdentifiers = [NSSet setWithObject:ADBannerContentSizeIdentifierPortrait];
    _bannerView.currentContentSizeIdentifier = ADBannerContentSizeIdentifierPortrait;
    [[[CCDirector sharedDirector]view]addSubview:_bannerView];
    [_bannerView setBackgroundColor:[UIColor clearColor]];
    [[[CCDirector sharedDirector]view]addSubview:_bannerView];
    _bannerView.delegate = self;
}



- (void)bannerViewDidLoadAd:(ADBannerView *)banner{
    [self layoutAnimated:YES];
}

- (void)layoutAnimated:(BOOL)animated{
    CGRect contentFrame = [CCDirector sharedDirector].view.bounds;
    CGRect bannerFrame = _bannerView.frame;
    if (_bannerView.bannerLoaded)
    {
        contentFrame.size.height -= _bannerView.frame.size.height;
        bannerFrame.origin.y = contentFrame.size.height;
    } else {
        bannerFrame.origin.y = contentFrame.size.height;
    }
    
    [UIView animateWithDuration:animated ? 0.25 : 0.0 animations:^{
        //_contentView.frame = contentFrame;
        //[_contentView layoutIfNeeded];
        _bannerView.frame = bannerFrame;
    }];
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error{
    [self layoutAnimated:YES];
}


- (BOOL)bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave{
    NSLog(@"Banner view is beginning an ad action");
    BOOL shouldExecuteAction = YES;//[[CCDirector sharedDirector].view allowActionToRun]; // your app implements this method
    
    if (!willLeave && shouldExecuteAction)
    {
        [[CCDirector sharedDirector] pause];
        // insert code here to suspend any services that might conflict with the advertisement
        [[SIAudioManager sharedAudioManager]pauseAudio];
    }
    return shouldExecuteAction;
}

-(void)bannerViewActionDidFinish:(ADBannerView *)banner{
    [[CCDirector sharedDirector] resume];
    [[SIAudioManager sharedAudioManager] resumeAudio];
}
/*
 - (void)layoutAnimated:(BOOL)animated
 {
 // As of iOS 6.0, the banner will automatically resize itself based on its width.
 // To support iOS 5.0 however, we continue to set the currentContentSizeIdentifier appropriately.
 //CGRect contentFrame = [CCDirector sharedDirector].view.bounds;
 CGRect contentFrame = [self boundingBox];
 
 if (contentFrame.size.width < contentFrame.size.height) {
 _bannerView.currentContentSizeIdentifier = ADBannerContentSizeIdentifierPortrait;
 } else {
 _bannerView.currentContentSizeIdentifier = ADBannerContentSizeIdentifierLandscape;
 }
 
 CGRect bannerFrame = CGRectZero;//_bannerView.frame;
 if (_bannerView.bannerLoaded) {
 contentFrame.size.height -= _bannerView.frame.size.height;
 bannerFrame.origin.y = contentFrame.size.height;
 } else {
 bannerFrame.origin.y = contentFrame.size.height;
 }
 
 [UIView animateWithDuration:animated ? 0.25 : 0.0 animations:^{
 _bannerView.frame = bannerFrame;
 }];
 }
 
 - (void)bannerViewDidLoadAd:(ADBannerView *)banner {
 [self layoutAnimated:YES];
 }
 
 - (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error {
 [self layoutAnimated:YES];
 }
 */


@end

