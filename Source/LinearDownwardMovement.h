//
//  LinearDownwardMovement.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 12/12/15.
//  Copyright © 2015 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PhysicsBodyMoveBehavior.h"
@interface LinearDownwardMovement : NSObject <PhysicsBodyMoveBehavior>

@end
