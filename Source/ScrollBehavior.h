//
//  ScrollBehavior.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 8/20/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ScrollBehavior <NSObject>

@property float scrollSpeed;

-(void)scrollNodes:(NSArray*)backgroundNodes withTime:(CCTime)delta;

@end
