//
//  EmptySetBehavior.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 12/12/15.
//  Copyright © 2015 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ItemOccurrenceSetBehavior.h"


@interface EmptySetBehavior : NSObject <ItemOccurrenceSetBehavior>



@end
