//
//  AdBanner.h
//  Shape Inrush
//
//  Created by Eliud Ortiz on 2/9/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "CCNode.h"
#import <iAd/iAd.h>

@interface AdBanner : NSObject <ADBannerViewDelegate>{
    
}

@property ADBannerView *bannerView;

+(AdBanner*)sharedAdBanner;
-(void)initBanner;

@end
