//
//  WeaponBehavior.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 8/25/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol WeaponBehavior <CCSchedulerTarget>

@property (weak) CCNode* weaponUser;

// weapon projectile loaded in ccb
@property (readwrite) NSString* weaponFile;
@property (readwrite) float fireRate;
@property int ammo;

// projectile offsets from the center of the object where projectile will spawn
@property NSMutableArray *gameObjectSpawners;
-(void)useWeapon;
-(id)initDefault:(CCNode*)weaponUser;

@end
