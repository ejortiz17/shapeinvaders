//
//  Gameplay.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 7/19/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "CCNode.h"

#import "Hero.h"
#import "ReplaceScene.h"
#import "StandardSceneReplacement.h"

@interface Gameplay : CCNode

@property (strong) id followAction ;
@property UILongPressGestureRecognizer *longPress; 
@property CGPoint touchPoint;
@property (nonatomic) int score;
@property id<ReplaceScene>sceneReplacer;

//@property Hero* hero;

-(void)touchInView;

-(void)showLeaderboard;
-(void)toMainScene;


@end
