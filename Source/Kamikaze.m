//
//  Kamikaze.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 1/22/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "Kamikaze.h"
#import "ZeroInMovement.h"
#import "UpperScreenOffSpawn.h"
@implementation Kamikaze


-(void)didLoadFromCCB{
    
    [super didLoadFromCCB];
    // change default spawn generator to top screen Spawn
    self.spawnPointGenerator = [[UpperScreenOffSpawn alloc]init];
    self.movementMultiplier = .5;
    
    // set physics collision properties
    self.physicsBody.collisionType = @"enemy_2";
    self.physicsBody.collisionCategories = @[@"enemies_2"];
    self.physicsBody.collisionMask = @[@"heroes",@"projectiles", @"enemies", @"enemies_2"];
    
    [self setMoveBehavior:[[ZeroInMovement alloc]init] ];
    [self.moveBehavior setBody:self.physicsBody];
 
    self.movementMultiplier = 2;
    self.hp = 3;
    self.invincible = false;
    self.isAttacking = false;
    self.weaponBehavior = nil;
    
    self.maxSpeedFactor = 1;
    
}

-(void)removeSelf{
    self.physicsBody = nil;
    [self removeFromParentAndCleanup:YES];
    CCLOG(@"Kamikaze Removed");
}

-(void)fixedUpdate:(CCTime)delta{
    [super fixedUpdate:delta];
    [self.moveBehavior setStartingPosition:self.position];
    
    if (self.hero != nil) {
        [self.moveBehavior setTargetPosition:ccpAdd(self.hero.position, ccp(0,50))];
    } else{
        [self.moveBehavior setTargetPosition:ccp(self.position.x, self.position.y-30)];
    }
    
    
    [self move];
}

-(void)move{
    
    [self.moveBehavior moveWithFactor:self.movementMultiplier];
}
@end

