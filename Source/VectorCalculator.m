//
//  VectorCalculator.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 7/19/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "VectorCalculator.h"

@implementation VectorCalculator

+(CGPoint)vectorFromStartPoint:(CGPoint)startPoint toEndPoint:(CGPoint)endPoint{
    
    // CGPoints assume an origin (0,0), so below is basic vector addition
    CGPoint negStartPoint = ccpMult(startPoint, -1);
    CGPoint vector = ccpAdd(endPoint, negStartPoint);
    
    return vector;
}

@end
