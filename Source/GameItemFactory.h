//
//  GameItemFactory.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 10/17/15.
//  Copyright © 2015 Apportable. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import "GameObjectTypes.h"
#import "GameItem.h"

@protocol GameItemFactory <NSObject>

-(GameItem*)makeGameItemFromType:(GameItemType)gameItemType;

@optional



@end
