//
//  DefaultWeaponBehavior.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 8/25/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "DefaultWeaponBehavior.h"
#import "GameProjectile.h"
#import "GameObjectSpawner.h"
#import "GameProjectileFactory.h"
#import "StringConstants.h"
@implementation DefaultWeaponBehavior 

@synthesize gameObjectSpawners = _gameObjectSpawners;
@synthesize weaponUser = _weaponUser;
@synthesize fireRate = _fireRate;
@synthesize ammo = _ammo;
@synthesize weaponFile = _weaponFile;


-(id)initWithSpawnPoints:(NSMutableArray*)spawnPoints weaponUser:(CCNode*)weaponUser{
    if (self = [super init]) {
        self.gameObjectSpawners  = spawnPoints;
        self.weaponUser = weaponUser;
        self.weaponFile = DEFAULT_PROJECTILE;
        self.fireRate = 3;
        self.ammo = 100;
    }
    return self;
};

-(id)initDefault:(CCNode *)weaponUser{
    if (self = [super init]) {
        
        self.weaponUser = weaponUser;
        
        GameObjectSpawner *spawner1 = [[GameObjectSpawner alloc] init];
        
        [weaponUser addChild:spawner1];

        spawner1.position = ccp(0, 80);
        spawner1.isActive = YES;
        spawner1.forceToApply = ccp(0,200);
        
        self.fireRate = 3;
        self.ammo = 100;
        
        
        NSMutableArray *spawners = [NSMutableArray arrayWithObject:spawner1];
        self.gameObjectSpawners = spawners;
        
        self.weaponFile = DEFAULT_PROJECTILE;
        
    }
    
    return self;
}



-(void)setFireRate:(float)fireRate{
    
    // limit fire rate to 7 rounds per second
    
    if (fireRate <= 0) {
        _fireRate = 1;
    }else if (fireRate > 7){
        _fireRate = 7;
    }else{
        _fireRate = fireRate;
    }
}

-(void)useWeapon{
    
    for (GameObjectSpawner* gameObjectSpawner in self.gameObjectSpawners) {
        // file the default prokectile
        if (gameObjectSpawner.isActive) {
            if (self.weaponFile == nil) {
                self.weaponFile = DEFAULT_PROJECTILE;
            }
            if (![self.weaponFile isEqualToString:DEFAULT_PROJECTILE] && self.ammo > 0) {
                self.ammo--;
                //[gameObjectSpawner spawnGameObjectFromCCB:self.weaponFile toLayer:_weaponUser.parent];
                [gameObjectSpawner spawnProjectileFromCCB:self.weaponFile toLayer:_weaponUser.parent shooter:(GameCharacter*)_weaponUser];
                [[NSNotificationCenter defaultCenter] postNotificationName:WEAPON_FIRE_NOTIFICATION object:nil userInfo:@{SOUND_EFFECTS_KEY:WEAPON_EFFECT_1}];

            } else if ([self.weaponFile isEqualToString:DEFAULT_PROJECTILE]){
                [gameObjectSpawner spawnProjectileFromCCB:self.weaponFile toLayer:_weaponUser.parent shooter:(GameCharacter*)_weaponUser];
                [[NSNotificationCenter defaultCenter] postNotificationName:WEAPON_FIRE_NOTIFICATION object:nil userInfo:@{SOUND_EFFECTS_KEY:WEAPON_EFFECT_2}];

            }
        }
    }
}

@end
