//
//  RandSIEnemyPicker.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 12/5/15.
//  Copyright © 2015 Apportable. All rights reserved.
//
//  Picks random Enemy out of available enmemy const
//

#import "RandSIEnemyPicker.h"

@implementation RandSIEnemyPicker{
    
    NSMutableArray* _objectsAndOccurrences;
}

@synthesize defaultSetBehavior = _defaultSetBehavior;
@synthesize defaultObjectOccurrenceAddition = _defaultObjectOccurrenceAddition;
@synthesize pickBehavior = _pickBehavior;


-(id)initDefault{
    self = [super init];
    
    if (self) {
        self.defaultSetBehavior = [[EmptySetBehavior alloc]init];
        _objectsAndOccurrences = [NSMutableArray array];
        [self.defaultSetBehavior setItemsAndOccurrences:_objectsAndOccurrences numOfItems:enemyTotal];
        
        self.defaultObjectOccurrenceAddition = [[StandardObjectOccurrenceAddition alloc]init];
        self.pickBehavior = [[StandardRandPickBehavior alloc]init];
        
        // set one Enemy to appear at first. Default to HEX
        [self.defaultObjectOccurrenceAddition addOccurrenceValue:1 toObjectType:kHexEnemy inCollection:_objectsAndOccurrences];
        [self.defaultObjectOccurrenceAddition addOccurrenceValue:1 toObjectType:kGruntEnemy inCollection:_objectsAndOccurrences];
        //[self.defaultObjectOccurrenceAddition addOccurrenceValue:3 toObjectType:kAntiHeroEnemy inCollection:_objectsAndOccurrences];
        
    }
    
    return self;
}

-(id)initUniform{
    
    self = [super init];
    
    if (self) {
        
        // set default behavior and set items and occurrences uniformly
        self.defaultSetBehavior = [[UniformSetBehavior alloc] init];
        _objectsAndOccurrences = [NSMutableArray array];
        [self.defaultSetBehavior setItemsAndOccurrences:_objectsAndOccurrences numOfItems:enemyTotal];
        
        self.pickBehavior = [[StandardRandPickBehavior alloc]init];
        
        self.defaultObjectOccurrenceAddition = [[StandardObjectOccurrenceAddition alloc]init];
        
    }
    
    return self;
    
}


// return the random gameObject
-(int)pickRandomObjectType{
    
    return [self.pickBehavior pickRandomObjectTypeinCollection:_objectsAndOccurrences];
}


-(void)addOccurrenceValue:(int)numToAdd toObjectType:(int)objectType{
    
    [self.defaultObjectOccurrenceAddition addOccurrenceValue:numToAdd toObjectType:objectType inCollection:_objectsAndOccurrences];
    
}
@end
