//
//  GCHelper.h
//  Shape Inrush
//
//  Created by Eliud Ortiz on 2/1/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GameKit/GameKit.h>

@interface GCHelper : NSObject{
    BOOL gameCenterAvailable;
    BOOL userAuthenticated;
}

@property (assign, readonly) BOOL gameCenterAvailable;

+(GCHelper*)sharedInstance;
-(void)authenticateLocalUser;

@end
