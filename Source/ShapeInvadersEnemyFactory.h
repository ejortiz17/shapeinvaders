//
//  ShapeInvadersEnemyFactory.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 12/5/15.
//  Copyright © 2015 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GameEnemyFactory.h"

@interface ShapeInvadersEnemyFactory : NSObject <GameEnemyFactory>


@end
