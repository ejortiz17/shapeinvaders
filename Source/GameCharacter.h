//
//  GameCharacter.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 7/19/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "GameObject.h"
#import "WeaponBehavior.h"
#import "PhysicsBodyMoveBehavior.h"
#import "DeathBehavior.h"

@interface GameCharacter : GameObject

+(GameCharacter *)newGameCharacterFromCCB:(NSString *)CCBFile position:(CGPoint)position velocity:(CGPoint)velocity;

@property int hp;
@property BOOL invincible;
@property int invincibleTimer;
@property BOOL takingDamage;
@property NSUInteger numOfAnimationsRunning;

// speed passed to movebehavior
@property CGFloat speedFactor;

// internally multiplies final movement speed
@property CGFloat movementMultiplier;

@property CGFloat maxSpeedFactor;

@property BOOL isAttacking;
@property id<WeaponBehavior>weaponBehavior;
@property id<PhysicsBodyMoveBehavior>moveBehavior;
@property id<DeathBehavior>deathBehavior;

-(void)move;

-(void)performDeathBehavior;

-(void)takeDamage:(int)damage;

-(void)useWeapon;


@end
