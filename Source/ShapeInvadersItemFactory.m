//
//  ShapeInvadersItemFactory.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 10/17/15.
//  Copyright © 2015 Apportable. All rights reserved.

// Hidden switch to return different game types
//

#import "ShapeInvadersItemFactory.h"

@implementation ShapeInvadersItemFactory

-(GameItem*)makeGameItemFromType:(GameItemType)gameItemType{
    
    // create blank gameItem
    GameItem *item = [[GameItem alloc]init];
    item = nil;
    
    // switch will be the GameItem type
    switch (gameItemType) {
        case kHealth1Item:
            item = (GameItem*)[CCBReader load:@"Health1"];
            break;
        case kStarWeaponItem:
            item = (GameItem*)[CCBReader load:@"StarProjectileItem"];
            break;
        case kInvincibilityStarItem:
            item = (GameItem*)[CCBReader load:@"InvincibilityStar"];
            break;
            
        case kFireRateIncreaseItem:
            item = (GameItem*)[CCBReader load:@"FireRateIncreaseItem"];
        default:
            break;
    }
    
    return item;
}


@end
