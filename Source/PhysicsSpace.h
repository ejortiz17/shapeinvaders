//
//  PhysicsSpace.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 8/25/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "CCPhysicsNode.h"

@interface PhysicsSpace : CCPhysicsNode <CCPhysicsCollisionDelegate>

@end
