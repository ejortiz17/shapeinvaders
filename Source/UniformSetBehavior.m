//
//  UniformSetBehavior.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 11/5/15.
//  Copyright © 2015 Apportable. All rights reserved.
//

#import "UniformSetBehavior.h"

@implementation UniformSetBehavior

-(void)setItemsAndOccurrences:(NSMutableArray *)itemsAndOccurrences numOfItems:(int)numOfItems{
    
    // set the occurrences of each item type (enum, but use as an int) and assign number of occurrences. In this case, 1 for each.
    for (int i = 0; i < numOfItems; i++) {
        
        NSNumber* numOccurrence = [NSNumber numberWithInt:1];
        [itemsAndOccurrences setObject:numOccurrence atIndexedSubscript:i];
    }
}

@end
