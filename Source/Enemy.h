//
//  Enemy.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 8/24/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "GameCharacter.h"
#import "SpawnPointGeneratorBehavior.h"
#import "TopScreenOffSpawn.h"
#import "SingleImpulseMovement.h"
#import "StandardExplosionDeath.h"
#import "Hero.h"


@interface Enemy : GameCharacter <SpawnPointGeneratorBehavior>

@property BOOL go;

// points that an enemy is worth on death
@property int pointBonus;
@property id<SpawnPointGeneratorBehavior>spawnPointGenerator;

// track the hero in case you need to update the movements to shoot at it 
@property(weak) Hero *hero;

-(void)postNotificationOnDeath;
@end
