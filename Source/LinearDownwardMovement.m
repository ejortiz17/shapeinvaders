//
//  LinearDownwardMovement.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 12/12/15.
//  Copyright © 2015 Apportable. All rights reserved.
//

#import "LinearDownwardMovement.h"
#import "VectorCalculator.h"

@implementation LinearDownwardMovement

@synthesize body;
@synthesize startingPosition;
@synthesize targetPosition;

-(void)moveWithFactor:(CGFloat)speed{
    
    // only apply 
    
    CGPoint velocityVec = [VectorCalculator vectorFromStartPoint:self.startingPosition toEndPoint:self.targetPosition];
    CGPoint velocityVecMult = ccpMult(velocityVec, speed);
    
    self.body.velocity = velocityVecMult;
}

@end
