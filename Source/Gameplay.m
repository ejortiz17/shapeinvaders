//
//  Gameplay.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 7/19/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "Gameplay.h"
#import "VectorCalculator.h"
#import "PhysicsSpace.h"
#import "NodeFollower.h"
#import "CCCustomFollow.h"
#import "Gameworld.h"
#import "ScrollingBackground.h"
#import "Enemy.h"
#import "GameProjectile.h"
#import "GameItem.h"

#import "StringConstants.h"

// item pickers
#import "ShapeInvadersItemFactory.h"
#import "RandSIItemPicker.h"

#import "ShapeInvadersEnemyFactory.h"
#import "RandSIEnemyPicker.h"

// menus

#import "PauseMenu.h"
#import "GameKitHelper.h"
#import "SIAudioManager.h"


#define DISTANCE_TRAVEL_UPDATE 1000
#define SCROLL_SPEED_INCREASE 20
#define MAX_SCROLL_SPEED 600

#define MIN_ITEM_SPAWNTIME 3
#define MIN_ENEMY_SPAWNTIME .25

#define ENEMY_UPDATE_TIME_DIFF .005
#define ITEM_UPDATE_TIME_DIFF .005


// constants used to increase the speed of spawning objects
#define TURNS_TO_HIT_MAX_SPEED 100
#define INIT_SPEED_FACTOR 60

// items

#define MAX_ITEM_SPEED_FACTOR 200

@implementation Gameplay{
    CCNode *_gameContentNode;
    Gameworld *_gameworld;
    PhysicsSpace *_physicsNode;
    CCNode *_menuNode;
    CCButton* _pauseButton;
    
    Hero *_hero;
    CCTimer *_timer;
    
    RandSIItemPicker *_randItemPicker;
    ShapeInvadersItemFactory *_itemFactory;
    
    RandSIEnemyPicker *_randEnemyPicker;
    ShapeInvadersEnemyFactory *_enemyFactory;

    
    CCNode *_gameOverLayer;
    
    GameItem *_heart;
    
    //PauseMenu *_pauseMenu;
    
    float _scrollSpeed;
    
    float _itemSpeedMultiplier;
    
    float _distanceTraveled;
    
    float _numOfDifficultyIncreases;
    
    // menu items
    CCNode* _healthblock1;
    CCNode* _healthblock2;
    CCNode* _healthblock3;
    
    int _score;
    
    BOOL _gameOver;
    BOOL _increaseScrollSpeed;
    
    CGFloat _randItemSpawnTime;
    CGFloat _randEnemySpawnTime;
    
    CCLabelTTF* _scoreLabel;

    
}

@synthesize followAction = _followAction;
@synthesize touchPoint  = _touchPoint;
@synthesize longPress = _longPress;
@synthesize score = _score;
//@synthesize hero = _hero;

-(void)didLoadFromCCB{
    
    self.userInteractionEnabled = YES;
    
    _itemSpeedMultiplier = 2.0;
    
    _scrollSpeed = INIT_SPEED_FACTOR; // init speed of the screen scrolling is 20px/update
    _distanceTraveled = 0;
    _numOfDifficultyIncreases = 0;
    
    _randEnemySpawnTime = .75;
    _randItemSpawnTime = 4;
    
    _randItemPicker = [[RandSIItemPicker alloc]initDefault];
    _itemFactory = [[ShapeInvadersItemFactory alloc]init];
    
    _randEnemyPicker =[[RandSIEnemyPicker alloc]initDefault];
    _enemyFactory = [[ShapeInvadersEnemyFactory alloc]init];
    
    //[self schedule:@selector(spawnEnemyOffScreen) interval:1.5];
    [self schedule:@selector(spawnRandomEnemy) interval:_randEnemySpawnTime];
    [self schedule:@selector(spawnRandomItem) interval:_randItemSpawnTime];
    
    [_gameworld.background.scrollBehavior setScrollSpeed:60];
    
    _gameOverLayer.visible = NO;
    _gameOverLayer.opacity = 0.0;
    
    self.sceneReplacer = [[StandardSceneReplacement alloc] init];
    
    /* Register notifications */
    
    //register hero death notifications
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gameOverAction) name:HERO_DEATH_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enemyDeathAction:) name:ENEMY_DEATH_NOTIFICATION object:nil];
    
    
    _score = 0;
    _gameOver = false;
    _increaseScrollSpeed = false;
    
    [[SIAudioManager sharedAudioManager]playBg:BG_MUSIC volume:[SIAudioManager sharedAudioManager].bgmVolume pan:0.0 loop:YES];

}

-(void)setScore:(int)score{
    if(!_gameOver){
        _score = score;
    }
    else{
        _score += 0;
    }
}

-(void)gameOverAction{
    
    CCLOG(@"GAME OVER!");
    _gameOver = YES;
    _gameOverLayer.visible = YES;
    [_gameOverLayer runAction:[CCActionFadeIn actionWithDuration:.5]];
    [[GameKitHelper sharedGameKitHelper] submitScore:_score category:HIGH_SCORE_LEADERBOARD_CATEGORY];
    
}

-(void)pausePlay{
    [PauseMenu pausePlay];
    [[SIAudioManager sharedAudioManager]pauseAudio];
}

-(void)enemyDeathAction:(NSNotification*)notification{
    
    // add to score on enemy death
    
    if (!_gameOver) {
        NSDictionary *scoreDict = [notification userInfo];
        NSNumber* pointsNum = [scoreDict valueForKey:POINT_BONUS_KEY];
        int pointsBonus = [pointsNum intValue];
        
        _score += pointsBonus;
    }

    
}

-(void)touchBegan:(CCTouch *)touch withEvent:(CCTouchEvent *)event{
    
    if (!_gameOver) {
        // set new target position of hero
        CGPoint newTargetPosition = [touch locationInNode:_gameContentNode];
        [_hero.moveBehavior setTargetPosition:newTargetPosition];
        
        [_hero schedule:@selector(useWeapon) interval:(1/_hero.weaponBehavior.fireRate)];
        _hero.isAttacking = YES;
    }

    

}

-(void)touchMoved:(CCTouch *)touch withEvent:(CCTouchEvent *)event{
    
    if (!_gameOver) {
        // set new target position of hero
        CGPoint newTargetPosition = [touch locationInNode:_gameContentNode];
        [_hero.moveBehavior setTargetPosition:newTargetPosition];

    }

}

-(void)touchEnded:(CCTouch *)touch withEvent:(CCTouchEvent *)event{
    
    if (!_gameOver) {
        if(_hero.isAttacking){
            [_hero unschedule:@selector(useWeapon)];
            _hero.isAttacking = NO;
        }
    }


}

-(void)touchCancelled:(CCTouch *)touch withEvent:(CCTouchEvent *)event{
    //CCLOG(@"Touch cancelled");
}


-(void)spawnRandomItem{
    
    
    GameItem *gameItem = [[GameItem alloc] init];
    gameItem = [_itemFactory makeGameItemFromType:[_randItemPicker pickRandomObjectType]];
    
    CGPoint spawnPoint = [gameItem generateRandSpawnPointInLayer:self ];
    
    gameItem.position = spawnPoint;
    
    //gameItem.speedFactor = _scrollSpeed;
    
    gameItem.speedFactor = INIT_SPEED_FACTOR + ((( MAX_ITEM_SPEED_FACTOR - INIT_SPEED_FACTOR)/ (TURNS_TO_HIT_MAX_SPEED/2) )* _numOfDifficultyIncreases);
    
    [gameItem.moveBehavior setStartingPosition:self.position];
    [gameItem.moveBehavior setTargetPosition:ccp(self.position.x, self.position.y-100)];
    
    [_physicsNode addChild:gameItem];
    
    [gameItem move];
    
    // set view node to screen so it can remove itself on exiting screen
    [gameItem setViewNode:_physicsNode.parent];
    
}


-(void)spawnRandomEnemy{
    
    Enemy *enemy = [[Enemy alloc]init];
    enemy = [_enemyFactory makeGameItemFromType:[_randEnemyPicker pickRandomObjectType]];
    
    CGPoint spawnPoint = [enemy generateRandSpawnPointInLayer:self];
    
    enemy.position = spawnPoint;
    enemy.hero = _hero;
    
    enemy.speedFactor = INIT_SPEED_FACTOR + (( (enemy.maxSpeedFactor - INIT_SPEED_FACTOR)/TURNS_TO_HIT_MAX_SPEED) * _numOfDifficultyIncreases);
    
    //enemy.speedFactor = _scrollSpeed;
        
    [_physicsNode addChild:enemy];
    
    [enemy move];
    
    //add the viewNode to enemy so it can update its status
    [enemy setViewNode:_physicsNode.parent];
}


/******* update scene label and menus **********/

-(void)fixedUpdate:(CCTime)delta{
    
    if (!_gameOver) {
        float distanceChange = ceil(delta * _scrollSpeed);
        self.score += distanceChange;
        
        _distanceTraveled += distanceChange;
    }
    
    [self updateDifficulty];
    
    [self updateHealthMeter];
    [self updateScoreLabel];
    [self updateMenu];
}

-(void)updateHealthMeter{
    
    if(_hero.hp >= 3){
        _healthblock1.visible =YES;
        _healthblock2.visible = YES;
        _healthblock3.visible = YES;
    }
    else if (_hero.hp == 2){
        _healthblock1.visible = YES;
        _healthblock2.visible = YES;
        _healthblock3.visible = NO;
    }
    else if (_hero.hp == 1){
        _healthblock1.visible = YES;
        _healthblock2.visible = NO;
        _healthblock3.visible = NO;
    }
    else if(_hero.hp <= 0){
        _healthblock1.visible = NO;
        _healthblock2.visible = NO;
        _healthblock3.visible = NO;
    }
    else{
        _healthblock1.visible = NO;
        _healthblock2.visible = NO;
        _healthblock3.visible = NO;    }
}

-(void)updateScoreLabel{
    _scoreLabel.string = [NSString stringWithFormat:@"%d",_score];
}

-(void)updateMenu{
    if (_gameOver) {
        _pauseButton.visible = NO;
    }
}



#pragma mark Difficulty update methods

-(void)updateDifficulty{
    if ((int)_distanceTraveled % DISTANCE_TRAVEL_UPDATE < 5 && _distanceTraveled > DISTANCE_TRAVEL_UPDATE /*&& _increaseScrollSpeed*/) {
        
        if (_scrollSpeed + SCROLL_SPEED_INCREASE <= MAX_SCROLL_SPEED) {
            _scrollSpeed = _scrollSpeed +SCROLL_SPEED_INCREASE;
        }
        _scrollSpeed += SCROLL_SPEED_INCREASE;
        [_gameworld.background.scrollBehavior setScrollSpeed:_scrollSpeed];
        
        [self unschedule:@selector(spawnRandomEnemy)];
        [self unschedule:@selector(spawnRandomItem)];
        
        [self updateHexOccurrence];
        [self updateGruntOccurrence];
        [self updateKamikazeOccurrence];
        [self updateAntiHeroOccurrence];
        
        
        if (_randEnemySpawnTime-ENEMY_UPDATE_TIME_DIFF > MIN_ENEMY_SPAWNTIME) {
            _randEnemySpawnTime -= ENEMY_UPDATE_TIME_DIFF;
        }
        
        if (_randItemSpawnTime-ITEM_UPDATE_TIME_DIFF > MIN_ITEM_SPAWNTIME) {
            _randItemSpawnTime -= ITEM_UPDATE_TIME_DIFF;
        }
        
        
        [self schedule:@selector(spawnRandomEnemy) interval:_randEnemySpawnTime];
        [self schedule:@selector(spawnRandomItem) interval:_randItemSpawnTime];
        //_increaseScrollSpeed = NO;
        _numOfDifficultyIncreases +=1;
        
        
    }
    /*
    if ( ((int)_distanceTraveled % DISTANCE_TRAVEL_UPDATE) >= 5 && _scrollSpeed <= MAX_SCROLL_SPEED) {
        _increaseScrollSpeed = YES;
    }
     */
}

-(void)updateHexOccurrence{
    if ((int)_distanceTraveled % DISTANCE_TRAVEL_UPDATE < 5 && _distanceTraveled > 4* DISTANCE_TRAVEL_UPDATE ) {
        [_randEnemyPicker addOccurrenceValue:1 toObjectType:kHexEnemy];
    }
}

-(void)updateGruntOccurrence{
    if ((int)_distanceTraveled % DISTANCE_TRAVEL_UPDATE < 5 && _distanceTraveled > 4* DISTANCE_TRAVEL_UPDATE ) {
        [_randEnemyPicker addOccurrenceValue:1 toObjectType:kGruntEnemy];
    }
}

-(void)updateKamikazeOccurrence{
    if ((int)_distanceTraveled % DISTANCE_TRAVEL_UPDATE < 5 && _distanceTraveled > 13* DISTANCE_TRAVEL_UPDATE ) {
        [_randEnemyPicker addOccurrenceValue:1 toObjectType:kKamikazeEnemy];
    }
}

-(void)updateAntiHeroOccurrence{
    if ((int)_distanceTraveled % DISTANCE_TRAVEL_UPDATE < 5 && _distanceTraveled > 8 * DISTANCE_TRAVEL_UPDATE ) {
        [_randEnemyPicker addOccurrenceValue:1 toObjectType:kAntiHeroEnemy];
    }
}







@end
