//
//  Enemy.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 8/24/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//
//  Generic Enemy Class

#import "Enemy.h"
#import "StringConstants.h"

@implementation Enemy

@synthesize pointBonus = _pointBonus;
@synthesize spawnPointGenerator = _spawnPointGenerator;

-(void)didLoadFromCCB{
    
    // set physics collision properties
    self.physicsBody.collisionType = @"enemy";
    self.physicsBody.collisionCategories = @[@"enemies"];
    self.physicsBody.collisionMask = @[@"heroes",@"projectiles", @"enemies_2"];
    
    
    [self setEnteredView:NO];
    [self setNowInView:NO];
    
    self.speedFactor = 50;
    self.movementMultiplier = 1;
    self.hp = 1;
    
    // by default, all enemies spawn off top of screen
    self.spawnPointGenerator = [[TopScreenOffSpawn alloc]init];
    
    // default to single impulse movement behavior going
    self.moveBehavior = [[SingleImpulseMovement alloc] init];
    [self.moveBehavior setBody:self.physicsBody];
    [self.moveBehavior setStartingPosition:self.position];
    [self.moveBehavior setTargetPosition:ccp(self.position.x, self.position.y-1)];
    
    // set up death behavior
    self.deathBehavior = [[StandardExplosionDeath alloc]init];
    [self.deathBehavior setDyingNode:self];
    
    // set pointbonus
    self.pointBonus = 50;
}


-(void)takeDamage:(int)damage{
    
    if (!self.invincible && damage >=1) {
        self.hp -= damage;
        
        // only run damaged animation if hp isn't depleted
        if (self.hp > 0) {
            //CCAnimationManager *animationManager = self.animationManager;
            //[animationManager runAnimationsForSequenceNamed:@"TakingDamage"];
        }
        
    }
}


-(void)removeSelf{
    self.physicsBody = nil;
    [self removeFromParentAndCleanup:YES];
    CCLOG(@"Enemy Removed");
    
}


-(void)fixedUpdate:(CCTime)delta{
    // check for object removal
    [self enteredViewCheck:self.viewNode];
    [self nowInViewCheck:self.viewNode];
    
    [self removalCheck];
    
    //
    if (self.hp <= 0) {
        [self postNotificationOnDeath];
        [self removeSelfAfterDestruction];
    }
    
}

// post notification upon death of hero
-(void)postNotificationOnDeath{
    
    // post the point bonus you get from killing an enemy
    NSString* key = POINT_BONUS_KEY;
    NSNumber* pointBonusValue = [NSNumber numberWithInt:self.pointBonus];
    NSDictionary* scoreDictionary = @{key:pointBonusValue, SOUND_EFFECTS_KEY:EXPLOSION};
    
    [[NSNotificationCenter defaultCenter]postNotificationName:ENEMY_DEATH_NOTIFICATION object:nil userInfo:scoreDictionary];
    [[NSNotificationCenter defaultCenter] postNotificationName:ENEMY_DEATH_NOTIFICATION object:nil];
}

-(CGPoint)generateRandSpawnPointInLayer:(CCNode *)layer{
    
    return [self.spawnPointGenerator generateRandSpawnPointInLayer:layer];
}

-(void)move{
    [self.moveBehavior moveWithFactor:self.speedFactor];
}


@end
