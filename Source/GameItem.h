//
//  GameItem.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 10/17/15.
//  Copyright © 2015 Apportable. All rights reserved.
//

#import "GameObject.h"
#import "WeaponBehavior.h"
#import "PhysicsBodyMoveBehavior.h"
#import "SpawnPointGeneratorBehavior.h"
#import "TopScreenOffSpawn.h"

#import "ModifyAttributesBehavior.h"

@interface GameItem : GameObject <SpawnPointGeneratorBehavior>


@property float speedFactor;
@property int hpBonus;
@property BOOL invincibility;
@property int invincibilityTime;
@property id<PhysicsBodyMoveBehavior>moveBehavior;
@property id<WeaponBehavior>weaponBehavior;
@property id<SpawnPointGeneratorBehavior>spawnPointGenerator;
@property id ammoBonus;

@property id<ModifyAttributesBehavior> gameCharModifier;




-(void)move;

-(void)modifyGameCharacter: (GameCharacter*)gameCharacter;

@end
