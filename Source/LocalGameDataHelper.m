//
//  LocalGameDataHelper.m
//  Shape Inrush
//
//  Created by Eliud Ortiz on 2/6/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "LocalGameDataHelper.h"

@implementation LocalGameDataHelper


// singleton 
+(LocalGameDataHelper*)sharedHelper{
    
    static LocalGameDataHelper *_sharedHelper = nil;
    
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedHelper = [[LocalGameDataHelper alloc] init];
    });
    
    return _sharedHelper;
}


// check if key is already set in NSUserDefaults
-(BOOL)isKeyValid:(NSString*)stringKey{
    if ([[NSUserDefaults standardUserDefaults]objectForKey:stringKey]!=nil) {
        return TRUE;
    }
    
    return FALSE;
}
@end
