//
//  UpperScreenOffSpawn.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 12/9/15.
//  Copyright © 2015 Apportable. All rights reserved.
//

#import "UpperScreenOffSpawn.h"

// spawns a point off of the upper half of the screen (off screen) and off the point

@implementation UpperScreenOffSpawn

-(CGPoint)generateRandSpawnPointInLayer:(CCNode *)layer{
    
    
    // give y a 50 percent chance to come from the top or the sides
    
    int y; // y coord
    int coinFlip = arc4random()%2;
    if (coinFlip == 0) {
        y = arc4random_uniform((layer.contentSizeInPoints.height));
    }
    else{
        y = layer.contentSizeInPoints.height + 30;
    }
    
    // let the x value be greater than the x value of the screen or less than
    int x;
    int xLowerBound;
    int xUpperBound;
    
    if (y >= layer.contentSizeInPoints.height) {
        xLowerBound = (layer.boundingBox.origin.x - 20);
        xUpperBound = (CGRectGetMaxX(layer.boundingBox))+20;
        x = xLowerBound + arc4random() % (xUpperBound - xLowerBound);
    }
    else{
        x = (CGRectGetMaxX(layer.boundingBox))+20;
        int rand = arc4random() % 2;
        if (rand == 0) {
            x = x *-1;
        }
    }
    return ccp(x, y);
    
    
    
}

@end
