//
//  IncreaseFireRate.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 1/22/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModifyAttributesBehavior.h"
#import "WeaponBehavior.h"
@interface IncreaseFireRate : NSObject <ModifyAttributesBehavior>

@end
