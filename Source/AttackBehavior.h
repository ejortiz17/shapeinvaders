//
//  AttackBehavior.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 7/19/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol AttackBehavior <NSObject>


// amount of damage done by an attack
@property int damageDealt;

// attacks by performing a certain action whether it be shooting or suicide moving
-(void)attack;
@end
