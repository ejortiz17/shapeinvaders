//
//  LocalGameDataHelper.h
//  Shape Inrush
//
//  Created by Eliud Ortiz on 2/6/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocalGameDataHelper : NSObject

-(BOOL)adsRemoved;

-(BOOL)isKeyValid:(NSString*)stringKey;

@end
