//
//  Background.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 8/20/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "CCNode.h"
#import "ScrollBehavior.h"

@interface Background : CCNode{
    id<ScrollBehavior>scrollBehavior;
}

// holds background images/nodes
@property NSArray *backgroundNodes;
@property (strong)id<ScrollBehavior>scrollBehavior;
@end
