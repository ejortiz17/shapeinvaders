//
//  DefaultProjectile.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 12/27/15.
//  Copyright © 2015 Apportable. All rights reserved.
//

#import "DefaultProjectile.h"

@implementation DefaultProjectile


-(void)didLoadFromCCB{
    
    [super didLoadFromCCB];
    self.damageStrength = 1;
    
    [self performSelector:@selector(blinkAnim) withObject:nil afterDelay:0.0];
}

-(void)blinkAnim{
    [self.animationManager runAnimationsForSequenceNamed:@"BlinkAnim"];
}

@end
