//
//  GameCharacter.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 7/19/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "GameCharacter.h"

@implementation GameCharacter{
    
}

@synthesize hp = _hp;
@synthesize takingDamage = _takingDamage;
@synthesize invincible = _invincible;
@synthesize invincibleTimer = _invincibleTimer;
@synthesize speedFactor = _speedFactor;
@synthesize movementMultiplier = _movementMultiplier;
@synthesize isAttacking = _isAttacking;
@synthesize weaponBehavior = _weaponBehavior;
@synthesize moveBehavior = _moveBehavior;
@synthesize deathBehavior = _deathBehavior;

+(GameCharacter *)newGameCharacterFromCCB:(NSString *)CCBFile position:(CGPoint)position velocity:(CGPoint)velocity{
    
    // create gameObject from CCBFile should it exist and initialize position and velocity
    GameCharacter* gameChar;
    @try {
        gameChar  = [CCBReader load:CCBFile];
    }
    @catch (NSException *exception) {
        CCLOG(@"File %@ does not exist.", CCBFile);
    }
    
    gameChar.position = position;
    
    if (gameChar.physicsBody != nil) {
        gameChar.physicsBody.velocity = velocity;
    }
    
    return gameChar;
    
}



// default to no movement
-(void)move{
    
}

-(void)useWeapon{
    if (self.weaponBehavior!= nil) {
        [self.weaponBehavior useWeapon];
    }
}

-(void)removeSelfAfterDestruction{
    [self performDeathBehavior];
    [self removeSelf];
    
}

-(void)performDeathBehavior{
    [self.deathBehavior performDeathBehavior];
}

-(void)takeDamage:(int)damage{
    
    if (!self.invincible && damage >=1) {
        self.hp -= 1;
    }
    
}

@end
