//
//  StoreHelper.m
//  Shape Inrush
//
//  Created by Eliud Ortiz on 2/9/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "StoreHelper.h"
#import "SIGameDataHelper.h"
#import "AdBanner.h"

@implementation StoreHelper{
    
    CCButton *_btnRemoveAds;
    CCButton *_btnRestorePurchases;
    CCButton *_btnMainScene;
    
}


- (void)didLoadFromCCB {
    
    SIGameDataHelper *gameDataHelper = [SIGameDataHelper sharedHelper];
    if ([gameDataHelper adsRemoved]) {
        /*
        _btnRemoveAds.enabled = FALSE;
        _btnRemoveAds.label.string = @"Ads Removed (PURCHASED!)";
        
        _btnRestorePurchases.enabled = FALSE;
        _btnRestorePurchases.label.string = @"Purchase Restored";
        */
        
        [self removeAds];
        
        
    }
    
}

#define kRemoveAdsProductIdentifier @"com.eliudortiz.SI.removeads"

// method hooked up to button
- (IBAction)tapsRemoveAds{
    CCLOG(@"User requests to remove ads");
    
    if([SKPaymentQueue canMakePayments]){
        CCLOG(@"User can make payments");
        
        SKProductsRequest *productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithObject:kRemoveAdsProductIdentifier]];
        productsRequest.delegate = self;
        [productsRequest start];
        
    }
    else{
        CCLOG(@"User cannot make payments due to parental controls");
        //this is called the user cannot make payments, most likely due to parental controls
    }
}

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response{
    SKProduct *validProduct = nil;
    int count = (int)[response.products count];
    if(count > 0){
        validProduct = [response.products objectAtIndex:0];
        CCLOG(@"Products Available!");
        [self purchase:validProduct];
    }
    else if(!validProduct){
        CCLOG(@"No products available");
        //this is called if your product id is not valid, this shouldn't be called unless that happens.
    }
}

- (IBAction)purchase:(SKProduct *)product{
    SKPayment *payment = [SKPayment paymentWithProduct:product];
    
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}

- (IBAction) restore{
    //this is called when the user restores purchases, you should hook this up to a button
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
    _btnRestorePurchases.label.string = @"Processing...";
}

- (void) paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue
{
    CCLOG(@"received restored transactions: %lu", (unsigned long)queue.transactions.count);
    for(SKPaymentTransaction *transaction in queue.transactions){
        if(transaction.transactionState == SKPaymentTransactionStateRestored){
            //called when the user successfully restores a purchase
            CCLOG(@"Transaction state -> Restored");
            
            [self removeAds];
            [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
            break;
        }
    }
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions{
    for(SKPaymentTransaction *transaction in transactions){
        switch(transaction.transactionState){
            case SKPaymentTransactionStatePurchasing: CCLOG(@"Transaction state -> Purchasing");
                //called when the user is in the process of purchasing, do not add any of your own code here.
                break;
            case SKPaymentTransactionStatePurchased:
                //this is called when the user has successfully purchased the package (Cha-Ching!)
                [self removeAds]; //you can add your code for what you want to happen when the user buys the purchase here, for this tutorial we use removing ads
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                CCLOG(@"Transaction state -> Purchased");
                break;
            case SKPaymentTransactionStateRestored:
                CCLOG(@"Transaction state -> Restored");
                //add the same code as you did from SKPaymentTransactionStatePurchased here
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                //called when the transaction does not finish
                if(transaction.error.code == SKErrorPaymentCancelled){
                    CCLOG(@"Transaction state -> Cancelled");
                    //the user cancelled the payment ;(
                }
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
        }
    }
}

-(void)removeAds{
    SIGameDataHelper* gameDataHelper = [SIGameDataHelper sharedHelper];
    
    _btnRemoveAds.enabled = FALSE;
    _btnRemoveAds.label.string = @"Ads Removed (PURCHASED!)";
    
    _btnRestorePurchases.enabled = FALSE;
    _btnRestorePurchases.label.string = @"Purchase Restored";
    
    [gameDataHelper setAdsRemoved:YES];
    ADBannerView *banner =  [AdBanner sharedAdBanner].bannerView;
    if (banner != nil) {
        [banner removeFromSuperview];
    }
    
    
}


-(void)toMainScene{
    
    CCScene *mainScene = [CCBReader loadAsScene:@"MainScene"];
    [[CCDirector sharedDirector]replaceScene:mainScene];
    
}

@end
