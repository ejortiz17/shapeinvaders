//
//  IncreaseFireRate.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 1/22/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "IncreaseFireRate.h"


@implementation IncreaseFireRate

@synthesize modData = _modData;

-(void)modifyGameCharacter:(GameCharacter *)gameCharacter{
    [self modifyWeapon:gameCharacter.weaponBehavior];
    
    // unschedule firing selector if attacking
    if (gameCharacter.isAttacking) {
        [gameCharacter unschedule:@selector(useWeapon)];
        [gameCharacter schedule:@selector(useWeapon) interval:1/gameCharacter.weaponBehavior.fireRate];
    }
}

-(void)modifyWeapon:(id<WeaponBehavior>)weaponBehavior{
    NSNumber* fireRate = [self.modData objectForKey:@"fireRate"];
    
    weaponBehavior.fireRate += [fireRate floatValue];
}


@end
