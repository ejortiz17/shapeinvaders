//
//  SIAudioManager.h
//  Shape Inrush
//
//  Created by Eliud Ortiz on 2/13/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "StringConstants.h"

@interface SIAudioManager : NSObject

@property BOOL bgMusicOn;
@property BOOL soundFxOn;

@property float sfxVolume;
@property float bgmVolume;

+(SIAudioManager*)sharedAudioManager;

-(void)setUp;

-(void)preloadEffect:(NSString*)filename;
-(void)preloadBg:(NSString*)bgMusic;
-(void)playBgWithLoop:(BOOL)loop;
-(void)playBg:(NSString*)filePath volume:(float)volume pan:(float)pan loop:(bool)loop;
-(void)stopBg;

//pause all audio
-(void)pauseAudio;
// unpause or resume all audio
-(void)resumeAudio;


-(void)playEffect:(NSString*)filePath;
-(void)playEffect:(NSString*)filePath volume:(float)volume pitch:(float)pitch pan:(float)pan loop:(bool)loop;
-(void)playEffectFromNotification:(NSNotification*)notification;
@end
