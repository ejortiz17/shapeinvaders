//
//  ChangeWeapon.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 1/18/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModifyAttributesBehavior.h"
#import "WeaponBehavior.h"

@interface ChangeWeapon : NSObject <ModifyAttributesBehavior>

-(void)modifyWeapon:(id<WeaponBehavior>)weaponBehavior;

@end
