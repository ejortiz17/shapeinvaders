//
//  FireRateIncreaseItem.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 1/22/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "FireRateIncreaseItem.h"
#import "IncreaseFireRate.h"

@implementation FireRateIncreaseItem

-(void)didLoadFromCCB{
    [super didLoadFromCCB];
    self.gameCharModifier = [[IncreaseFireRate alloc] init];
    self.gameCharModifier.modData = [NSDictionary dictionaryWithObjectsAndKeys:
                                     [NSNumber numberWithFloat:1], @"fireRate",
                                     nil];
}
@end
