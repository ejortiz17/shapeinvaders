//
//  TopScreenOffSpawn.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 12/9/15.
//  Copyright © 2015 Apportable. All rights reserved.
//
//  Spawn a point in the upper part of the string on the first half
//

#import <Foundation/Foundation.h>
#import "SpawnPointGeneratorBehavior.h"


@interface TopScreenOffSpawn : NSObject <SpawnPointGeneratorBehavior>


@end
