//
//  InvincibilityStar.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 1/13/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "InvincibilityStar.h"
#import "MakeInvincible.h"

@implementation InvincibilityStar

-(void)didLoadFromCCB{
    [super didLoadFromCCB];
    self.gameCharModifier = [[MakeInvincible alloc]init];
}

@end
