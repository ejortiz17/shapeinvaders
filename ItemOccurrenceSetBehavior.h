//
//  ItemOccurrenceSetBehavior.h
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 11/5/15.
//  Copyright © 2015 Apportable. All rights reserved.
//
//  Sets an NSMutableArray of itemsAndOccurrences to total set of 
//
#import <Foundation/Foundation.h>

@protocol ItemOccurrenceSetBehavior <NSObject>

-(void)setItemsAndOccurrences:(NSMutableArray*)itemsAndOccurrences numOfItems:(int)numOfItems;

@end
