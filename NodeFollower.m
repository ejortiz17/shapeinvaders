//
//  NodeFollower.m
//  ShapeInvaders
//
//  Created by Eliud Ortiz on 7/21/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "NodeFollower.h"
#import "VectorCalculator.h"

@implementation NodeFollower

@synthesize layerToMove = _layerToMove;
@synthesize nodeToFollow = _nodeToFollow;

+(void)centerLayer:(CCNode *)layer aroundNode:(CCNode *)node{
    
    // get screen width and height
    CGRect screenBounds = [UIScreen mainScreen].bounds;
    CGFloat screenWidth = screenBounds.size.width;
    CGFloat screenHeight = screenBounds.size.height;
    
    CGPoint nodePosInPoints = node.positionInPoints;
    
    // calculate the max x distance and the max y distance that camera will center on
    // this keeps the view within bounds of the layer
    int x = MAX(nodePosInPoints.x, screenWidth/2);
    int y = MAX(nodePosInPoints.y, screenHeight/2);
    
    x = MIN(x, layer.boundingBox.size.width - screenWidth/2);
    y = MIN(y, layer.boundingBox.size.height - screenHeight/2);
    
    CGPoint targetPosition = ccp(x,y);
    CGPoint viewCenter = ccp(screenWidth/2, screenHeight/2);
    CGPoint newLayerPosition = ccpSub(viewCenter, targetPosition);
    
    [layer.parent convertToNodeSpace:newLayerPosition];
    
    layer.position = newLayerPosition;
    
}

+(void)centerWorld:(CCNode *)world onChildNode:(CCNode *)nodeToFollow withSceneLayer:(CCNode *)sceneLayer{
    
    CGPoint nodeToFollowPosInScence = [sceneLayer convertPositionFromPoints:
                                       [sceneLayer convertToNodeSpace:nodeToFollow.position] type:sceneLayer.positionType];
    
    CGPoint worldPosInScene = [sceneLayer convertPositionFromPoints:
                               [sceneLayer convertToNodeSpace:world.position] type:sceneLayer.positionType];
    // the vector we will use to shift the position of the world
    CGPoint worldShiftVector = [VectorCalculator vectorFromStartPoint:worldPosInScene toEndPoint:nodeToFollowPosInScence];
    CGPoint worldShiftVectorConverted = [world.parent convertPositionFromPoints:[world.parent convertToNodeSpace:worldShiftVector] type:world.parent.positionType];
    
    //world.position = ccpAdd(world.position, worldShiftVectorConverted);
    world.position = ccp(worldPosInScene.x - nodeToFollowPosInScence.x, worldPosInScene.x - nodeToFollowPosInScence.x);
}

+(void)centerWorld:(CCNode *)world onChildNode:(CCNode *)targetNode{
    
    // convert both the world and target node positions relative to the scene
    CGPoint targetNodePosInScene = [world convertToWorldSpace:targetNode.positionInPoints];
    CGPoint worldPosInScene = [world.parent convertToWorldSpace:world.positionInPoints];
    
    // the vector that the world will shift by
    CGPoint worldShiftVec = [VectorCalculator vectorFromStartPoint:targetNodePosInScene toEndPoint:worldPosInScene];
    
    CGPoint targetPosInScene = ccpSub(worldPosInScene, worldShiftVec);
    
    // calculate the max boundary the screen should scroll; the screen should not scroll if the position hits the boundary
    CGSize worldSize = world.contentSizeInPoints;
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    
    CGRect worldBoundingBox = world.boundingBox;
    CGFloat worldMinX = CGRectGetMinX(worldBoundingBox);
    CGFloat worldMinY = CGRectGetMinY(worldBoundingBox);
    CGFloat worldMaxX = CGRectGetMaxX(worldBoundingBox);
    CGFloat worldMaxY  = CGRectGetMaxY(worldBoundingBox);
    
    // find if the smallest bounds between the screen size and the world size
    CGFloat viewSizeX = MIN(worldSize.width, screenSize.width);
    CGFloat viewSizeY = MIN(worldSize.height, screenSize.height);
    
    
    // find the max distance the camera will center on between the target position and bounds
    CGFloat newWorldPosX = MAX(worldMinX + viewSizeX/2, targetPosInScene.x);
    CGFloat newWorldPosY = MAX(worldMinY + viewSizeY/2, targetPosInScene.y);
    
    newWorldPosX = MIN(newWorldPosX, worldMaxX - viewSizeX/2);
    newWorldPosY = MIN(newWorldPosY, worldMaxY - viewSizeY/2);
    
    CGPoint viewCenter = ccp(viewSizeX/2, viewSizeY/2);
    CGPoint adjustedTargetPos = ccp(newWorldPosX, newWorldPosY);
    
    CGPoint shiftVec = [VectorCalculator vectorFromStartPoint:adjustedTargetPos toEndPoint:viewCenter];
    
    CGPoint newWorldPosInScene = ccpAdd(viewCenter, shiftVec);
    // convert world coordinates back to its parent coordinate system and the world's original CCPositionType
    CGPoint newWorldPos = [world.parent convertPositionFromPoints:[world.parent convertToNodeSpace:newWorldPosInScene]
                                                             type:world.positionType];
    world.position = newWorldPos;
    
}

+(void)centerViewOnLayer:(CCNode*)layer withPointInLayer:(CGPoint)posInLayer{
    
    // convert both the world and target node positions relative to the scene
    CGPoint targetPosInScene = [layer convertToNodeSpace:posInLayer];
    CGPoint layerPosInScene = [layer.parent convertToWorldSpace:posInLayer];
    
    // get the adjusted shift position for the layer
    CGPoint adjustedLayerPos = [NodeFollower adjustLayerPositionInPoints:targetPosInScene forViewScreen:[UIScreen mainScreen] andBounds:layer.boundingBox];
    
    CGPoint viewCenter = ccp(CGRectGetMidX([UIScreen mainScreen].bounds), CGRectGetMidY([UIScreen mainScreen].bounds));
    // find new position and convert back to original layer coordinates
    
    CGPoint shift = [VectorCalculator vectorFromStartPoint:adjustedLayerPos toEndPoint:viewCenter];
    
    CGPoint finalPosInScene = ccpAdd(viewCenter, shift);
    
    CGPoint newLayerPos = [layer.parent convertPositionFromPoints:[layer.parent convertToNodeSpace:finalPosInScene] type:layer.positionType];
    
    layer.position = newLayerPos;
}

// @brief Adjusts a layer's position so that the view contains the point and is entirely within the bounds
//(normally of a layer)
+(CGPoint)adjustLayerPositionInPoints:(CGPoint)position forViewScreen:(UIScreen*)viewScreen andBounds:(CGRect)boundsRect{
    
    
    CGFloat viewScreenWidth = viewScreen.bounds.size.width;
    CGFloat viewScreenHeight = viewScreen.bounds.size.height;
    
    CGFloat boundsRectMinX = CGRectGetMinX(boundsRect);
    CGFloat boundsRectMinY = CGRectGetMinY(boundsRect);
    CGFloat boundsRectMaxX = CGRectGetMaxX(boundsRect);
    CGFloat boundsRectMaxY = CGRectGetMaxY(boundsRect);
    
    CGFloat leftBound = boundsRectMinX + viewScreenWidth/2;
    
    // make sure the position makes the view stary to the right or top of the bounds
    CGFloat newXPos = MAX(CGRectGetMinX(boundsRect)+viewScreenWidth/2, position.x);
    CGFloat newYPos = MAX(CGRectGetMinY(boundsRect)+viewScreenHeight/2, position.y);
 
    // make sure the position is readjusted so the view stays to the left or top of the bounds
    newXPos = MIN(newXPos, CGRectGetMaxX(boundsRect)-viewScreenWidth/2);
    newYPos = MIN(newYPos, CGRectGetMaxY(boundsRect)-viewScreenHeight/2);
    
    return ccp(newXPos, newYPos);
}
@end
